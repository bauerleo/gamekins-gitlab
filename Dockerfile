FROM maven:3-eclipse-temurin-11-alpine
WORKDIR /
ADD target/gamekins-gitlab-0.5.1-SNAPSHOT-jar-with-dependencies.jar gamekins-gitlab.jar
ADD target/lib /lib
CMD java -jar gamekins-gitlab.jar