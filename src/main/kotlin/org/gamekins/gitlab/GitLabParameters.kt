package org.gamekins.gitlab

import org.gitlab4j.api.GitLabApi

class GitLabParameters(
    var classesPath: String = "",
    var jacocoResultsPath: String = "",
    var gitLabUrl: String = "",
    var accessToken: String = "",
    var projectId: String = "",
    var isBuildSuccessful: Boolean = true,
    var jobUrl: String = "",
    var projectUrl: String = "",
    var junitResultsPath: String,
    var basePath: String,
    var committerId: String,
    var apiUserPartofLeaderboard: Boolean
){
    val gitLabApi: GitLabApi
        get() = GitLabApi(this.gitLabUrl, this.accessToken)
}