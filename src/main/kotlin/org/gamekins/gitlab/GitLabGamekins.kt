package org.gamekins.gitlab

import org.eclipse.jgit.lib.PersonIdent
import org.gamekins.gitlab.challenge.*
import org.gamekins.gitlab.challenge.quest.GQuestFactory
import org.gitlab4j.api.GitLabApi
import org.gamekins.gitlab.interaction.GitInteraction
import org.gamekins.gitlab.gitlab.*
import org.gamekins.gitlab.gitlab.GitLabIssue
import org.gitlab4j.api.Constants
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import kotlin.math.max
import kotlin.math.min

const val MIN_CHALLENGES = 3
const val CHALLENGES_PER_USER = 2
const val MAX_CHALLENGES = 6

const val MIN_QUESTS = 1
const val QUESTS_PER_USER = 1
const val MAX_QUESTS = 2

const val CHALLENGE_ISSUE_LABEL = "gamekins: challenge"
const val QUEST_ISSUE_LABEL = "gamekins: quest"

class GitLabGamekins(
    private val api: GitLabApi, private val project: String, private val parameters: GitLabParameters
) {

    /**
     * Iterate over all users and generate new challenges if needed.
     */
    fun execute() {
        val users = getAllUsers(api, project)
        val apiUser = getMe(api)
        val store = GStore.initializeStore(api, parameters)
        val usersToAdd = if(parameters.apiUserPartofLeaderboard) users else users - apiUser
        store.content.users.addAll(usersToAdd)
        val gitDir = Paths.get(parameters.basePath, "/.git")
        val committer = findCommitter(gitDir, store.content.users)
        store.content.users.add(committer)
        parameters.committerId = committer.id.toString()
        val headCommit = GitInteraction.getHeadCommit(gitDir)

        // Check now solved and now unsolvable challenges
        // Remove challenge & issues for solved and unsolvable challenges
        val rejected = filterForRejectedChallenges(parameters, store.content)
        store.content.openChallenges.removeAll(rejected)
        val stepIsSolved = filterForStepIsSolvedQuests(store.content)
        val solved = filterForSolvedChallenges(store.content)
        store.content.openChallenges.removeAll(solved)
        val unsolvable = filterForUnSolvableChallenges(store.content)
        store.content.openChallenges.removeAll(unsolvable)

        // Update GitLab Issues
        solved.forEach { GitLabIssue.closeIssueForSolved(it, committer, headCommit.name, parameters) }
        rejected.forEach { GitLabIssue.commentIssueForRejected(it, parameters)}
        unsolvable.forEach { GitLabIssue.closeIssueForUnsolvable(it, headCommit.name, parameters) }
        stepIsSolved.forEach { GitLabIssue.updateIssueForSolvedStep(it, committer, headCommit.name, parameters, QUEST_ISSUE_LABEL)}

        // Award points for challenges
        store.content.users.remove(committer)
        val winner = committer.copy(score = committer.score + solved.sumOf { it.getScore() })
            .apply { this.solvedChallengeIds.addAll(solved.map { it.id }) }
        store.content.users.add(winner)

        // Generate new challenges with issues
        val generatedChallenges = generateChallenges(parameters, store.content)
        val generatedQuests = generateQuests(parameters, store.content)
        store.content.openChallenges.addAll(generatedChallenges + generatedQuests)
        println("[Gamekins] Open Challenges and Quests:")
        store.content.openChallenges.forEach { println("[Gamekins] - ${it.title}. Issue Id: #${it.id}") }

        // Update Leaderboard and persist
        GLeaderboard.updateLeaderBoard(store.content, parameters)
    }

    private fun filterForSolvedChallenges(store: GStoreContent): List<GChallenge> {
        println("[Gamekins] Searching for solved challenges:")

        val solved = store.openChallenges.filter { it.isSolved(parameters) }
        solved.forEach { println("[Gamekins] Solved challenge ${it.title}. Issue Id: #${it.id}") }

        return solved
    }

    private fun filterForUnSolvableChallenges(store: GStoreContent): List<GChallenge> {
        println("[Gamekins] Searching for unsolvable challenges:")

        // We cannot determine unsolvability if the build is unsuccessful
        if(!this.parameters.isBuildSuccessful) {
            println("[Gamekins] Cannot check unsolvability as build failed, all challenges remain solvable ")
            return store.openChallenges
        }

        val unsolvable = store.openChallenges.filterNot { it.isSolvable(this.parameters) }
        unsolvable.forEach { println("[Gamekins] Unsolvable challenge to delete ${it.title}. Issue Id: #${it.id}") }

        return unsolvable
    }

    private fun filterForRejectedChallenges(parameters: GitLabParameters, store: GStoreContent): List<GChallenge> {
        println("[Gamekins] Searching for rejected challenges:")

        // Open Challenges whose issues have been closed or deleted are rejected
        val api = parameters.gitLabApi.issuesApi

        val rejected = store.openChallenges.map {
            Pair(
                it, try {
                    api.getIssue(parameters.projectId, it.id.toLong()).state
                } catch (e: Exception) {
                    System.err.println("[Gamekins] Could not fetch issue #${it.id}")
                    Constants.IssueState.CLOSED
                }
            )
        }.filter { it.second == Constants.IssueState.CLOSED }.map { it.first }
        return rejected
    }

    private fun filterForStepIsSolvedQuests(store: GStoreContent): List<GQuest> {
        println("[Gamekins] Searching for quests with solved current step")

        val solvedStep = store.openChallenges
            .filterIsInstance<GQuest>()
            .filter { it.stepIsSolved(this.parameters) }
        solvedStep.forEach { println("[Gamekins] Step ${it.currentChallengeIndex + 1} solved in quest ${it.title}. Issue Id: #${it.id}") }
        return solvedStep
    }

    /**
     * Creates new challenges and returns a list of generated challenges and their Issue id.
     */
    private fun generateChallenges(parameters: GitLabParameters, store: GStoreContent): List<GChallenge> {
        val challenges = mutableListOf<GChallenge>()

        // Generate Build Challenge if Build is unsuccessful
        if (!parameters.isBuildSuccessful) {
            val buildChallenge = GChallengeFactory.generateBuildChallenge(
                store.openChallenges
            )
            if (buildChallenge != null) {
                challenges.add(buildChallenge)
            }
        }

        // Generate challenges
        val openCount = store.openChallenges.count()
        val userCount = store.users.count()
        var toGenerate = min(MAX_CHALLENGES, CHALLENGES_PER_USER * userCount)
        toGenerate = max(MIN_CHALLENGES, toGenerate) - openCount - challenges.count()
        val generated = GChallengeFactory.generateNChallenges(toGenerate, parameters, store.openChallenges)
        challenges.addAll(generated)

        // Create issues for Challenges
        challenges.forEach { it.id = GitLabIssue.createIssue(it, parameters, CHALLENGE_ISSUE_LABEL).iid.toString() }

        // Return challenges
        challenges.forEach { println("[Gamekins] Generated challenge: ${it.title}. Issue Id: #${it.id}") }
        return challenges
    }

    private fun generateQuests(parameters: GitLabParameters, store: GStoreContent): List<GQuest> {
        // Generate Quests
        val openCount = store.openChallenges.filter { it is GQuest }.count()
        val userCount = store.users.count()
        var toGenerate = min(MAX_QUESTS, QUESTS_PER_USER * userCount)
        toGenerate = max(MIN_QUESTS, toGenerate) - openCount
        val quests = GQuestFactory.generateNQuests(toGenerate, parameters, store.openChallenges)

        // Create issues for Challenges
        quests.forEach { it.id = GitLabIssue.createIssue(it, parameters, QUEST_ISSUE_LABEL).iid.toString() }

        // Return quests
        quests.forEach { println("[Gamekins] Generated quest: ${it.title}. Issue Id: #${it.id}") }
        return quests
    }

    private fun findCommitter(gitDir: Path, users: Collection<GitLabUser>): GitLabUser {
        val committer: PersonIdent = try {
            GitInteraction.getHeadCommit(gitDir).authorIdent
        } catch (e: Exception) {
            System.err.println("[Gamekins] Could not get head commit information from .git. User info is wrong!")
            PersonIdent("undefined", "undefined")
        }

        val user = users.firstOrNull {
            it.name == committer.name || it.userName == committer.name
        } ?: GitLabUser(
            avatarUrl = "https://avatars.dicebear.com/api/identicon/${committer.name.replace("\\s+".toRegex(), "")}.svg",
            isProjectMember = false,
            createdAt = Date(),
            email = committer.emailAddress,
            id = (Integer.MIN_VALUE..(-1)).random().toLong(),
            name = committer.name,
            state = "git",
            userName = "",
            webUrl = "",
        )

        println("[Gamekins] Committer is $user")
        return user
    }
}