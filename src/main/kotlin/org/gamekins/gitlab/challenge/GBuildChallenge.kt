package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import java.util.*

data class GBuildChallenge(
    override val created: Long,
    override var id: String,
    ) : GChallenge {

    override val title: String
        get() = "\uD83D\uDCA5 Fix the Build for ${getScorePointsStr()}!"

    override val description: String
        get() = "### The Build failed! \n\n" +
            "*Fix the build for 1 Point!*"

    override fun isSolvable(parameters: GitLabParameters): Boolean {
        return true
    }

    override fun isSolved(parameters: GitLabParameters): Boolean {
        return parameters.isBuildSuccessful
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is GBuildChallenge) return false
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }
    @JsonIgnore
    override fun getScore(): Int {
        return 1
    }

    override fun toString(): String {
        return title
    }
}