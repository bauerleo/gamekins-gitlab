package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.JUnitInteraction
import java.util.*

data class GTestCountChallenge(
    override val created: Long,
    override var id: String,
    val initialTestCount: Int

) : GChallenge {

    override val title: String
        get() = "\uD83E\uDDEA Write another Test for ${getScorePointsStr()}"

    override val description: String
        get() = "Currently the Project has $initialTestCount Tests.\n\n **Write another one for 1 Point!**"

    /**
     * This challenge is always solvable as you can always write more tests.
     */
    override fun isSolvable(parameters: GitLabParameters): Boolean {
        return true
    }

    /**
     * The challenge is solved the project contains more tests than [initialTestCount]
     */
    override fun isSolved(parameters: GitLabParameters): Boolean {
        val testCount: Int
        try {
            testCount = JUnitInteraction.calcExecutedTestsFromJUnitXml(parameters)
        } catch (e: java.lang.Exception) {
            System.err.println("[Gamekins] Problem accessing JUnit Test files: ${e.message}")
            return false
        }
        return parameters.isBuildSuccessful && testCount > initialTestCount
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is GTestCountChallenge) return false
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }

    @JsonIgnore
    override fun getScore(): Int {
        return 1
    }

    override fun toString(): String {
        return title
    }
}