package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import org.gamekins.gitlab.challenge.quest.GMultiChallengeQuest
import org.gamekins.gitlab.GitLabParameters

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(value = GBuildChallenge::class, name = "GBuildChallenge"),
    JsonSubTypes.Type(value = GTestCountChallenge::class, name = "GTestCountChallenge"),
    JsonSubTypes.Type(value = GClassCoverageChallenge::class, name = "GClassCoverageChallenge"),
    JsonSubTypes.Type(value = GMethodCoverageChallenge::class, name = "GMethodCoverageChallenge"),
    JsonSubTypes.Type(value = GLineCoverageChallenge::class, name = "GLineCoverageChallenge"),
    JsonSubTypes.Type(value = GMultiChallengeQuest::class, name = "GQuestChallenge"),
    JsonSubTypes.Type(value = GSmellChallenge::class, name = "GSmellChallenge")
)
interface GChallenge {

    override fun equals(other: Any?): Boolean

    var id: String

    /**
     * Returns the creation time in milliseconds since 01.01.1970.
     */
    val created: Long

    /**
     * Returns the name of the category of the current [Challenge].
     */
    @get:JsonIgnore
    val title: String

    /**
     * Description of the Issue
     */
    @get:JsonIgnore
    val description: String

    /**
     * Returns the highlighted file content underneath the [Challenge] like the source code.
     */
    @JsonIgnore
    fun getHighlightedFileContent(): String {
        return ""
    }

    /**
     * Returns the score for the specific [Challenge].
     */
    fun getScore(): Int

    @JsonIgnore
    fun getScorePointsStr(): String {
        return if (getScore() == 1) "1 Point"
        else "${getScore()} Points"
    }

    /**
     * Checks whether the current [Challenge] is still solvable or not.
     */
    fun isSolvable(parameters: GitLabParameters)
            : Boolean

    /**
     * Checks whether the current [Challenge] is solved.
     */
    fun isSolved(parameters: GitLabParameters)
            : Boolean

    /**
     * Returns the String representation of the [Challenge].
     */
    override fun toString(): String
}

interface GQuest : GChallenge {
    var assigneeId: String?

    val currentChallengeIndex: Int
    fun stepIsSolved(parameters: GitLabParameters): Boolean
}