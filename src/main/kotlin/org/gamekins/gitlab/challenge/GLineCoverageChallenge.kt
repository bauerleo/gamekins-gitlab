package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.CoverageInformation
import org.gamekins.gitlab.interaction.JacocoInteraction
import java.text.NumberFormat
import java.util.Objects

data class GLineCoverageChallenge(
    override val created: Long,
    override var id: String,
    val classPackage: String,
    val className: String,
    val lineNumber: Int,
    val initialClassCoverage: CoverageInformation,
    val initialLineCoverage: CoverageInformation
) : GChallenge {

    override val title: String
        get() = "✏️ Cover line $lineNumber in $classPackage.$className for ${getScorePointsStr()}!"

    override val description: String
        get() =
            "### Cover line $lineNumber in $classPackage.$className! \n\n" +
                    "**Cover the line for ${getScorePointsStr()}!** \n\n" +
                    "**Current Class Coverage:** ${
                        NumberFormat.getPercentInstance().format(initialClassCoverage.coverage)
                    }\n\n" +
                    "```java\n//line ${initialLineCoverage.startLine}\n${initialLineCoverage.sourceCode}\n```\n\n" +
                    "See the [Coverage Report](${initialLineCoverage.reportUrl})"

    /**
     * Unsolvable if Jacoco File does not exist or every line is covered
     */
    override fun isSolvable(parameters: GitLabParameters): Boolean {
        val jacocoFile = JacocoInteraction.getJacocoSourceFile(classPackage, className, parameters) ?: return false
        val cov =
            JacocoInteraction.parseLineCoverageInformation(jacocoFile, classPackage, className, lineNumber, parameters)
                ?: return false
        return cov.missed > 0 && cov.coverage < 1
    }

    /**
     * The challenge is solved if the coverage percentage has increased
     */
    override fun isSolved(parameters: GitLabParameters): Boolean {
        val jacocoFile = JacocoInteraction.getJacocoSourceFile(classPackage, className, parameters) ?: return false
        val cov =
            JacocoInteraction.parseLineCoverageInformation(jacocoFile, classPackage, className, lineNumber, parameters)
                ?: return false

        return cov.coverage > initialLineCoverage.coverage

    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is GLineCoverageChallenge) return false
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }

    @JsonIgnore
    override fun getScore(): Int {
        return if (initialClassCoverage.coverage > 0.8) 2 else 1
    }

    override fun toString(): String {
        return title
    }
}