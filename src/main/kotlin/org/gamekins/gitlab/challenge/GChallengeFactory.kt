package org.gamekins.gitlab.challenge

import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.JUnitInteraction
import org.gamekins.gitlab.interaction.JacocoInteraction
import org.gamekins.gitlab.interaction.SonarLintInteraction
import org.gamekins.gitlab.interaction.SourceFileInteraction
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import kotlin.collections.ArrayList

val challengeWeights = listOf(
    1 to GTestCountChallenge::class.toString(),
    2 to GClassCoverageChallenge::class.toString(),
    3 to GMethodCoverageChallenge::class.toString(),
    4 to GLineCoverageChallenge::class.toString(),
    4 to GSmellChallenge::class.toString()
)

object GChallengeFactory {

    fun generateNChallenges(
        toGenerate: Int, parameters: GitLabParameters, openChallenges: List<GChallenge>
    ): List<GChallenge> {
        var tries = 100
        val result = ArrayList<GChallenge>()

        while (tries > 0 && result.size < toGenerate) {
            val c = generateSingleChallenge(parameters, openChallenges + result)
            if (c != null) {
                result.add(c)
            }
            tries--
        }
        return result
    }

    private fun generateSingleChallenge(parameters: GitLabParameters, openChallenges: List<GChallenge>): GChallenge? {
        // Get weighted challenge by binary searching aggregated weights
        val tree = TreeMap<Double, String>()
        var count = 0.0
        challengeWeights.forEach {
            tree[count] = it.second
            count += it.first
        }
        val rnd = ThreadLocalRandom.current().nextDouble() * count
        val clazz = tree.floorEntry(rnd).value

        // Create challenge based on challenge type
        val challenge = when (clazz) {
            GTestCountChallenge::class.toString() -> generateTestCountChallenge(parameters, openChallenges)
            GClassCoverageChallenge::class.toString() -> generateClassCoverageChallenge(parameters, openChallenges)
            GMethodCoverageChallenge::class.toString() -> generateMethodCoverageChallenge(parameters, openChallenges)
            GLineCoverageChallenge::class.toString() -> generateLineCoverageChallenge(parameters, openChallenges)
            GSmellChallenge::class.toString() -> generateSmellChallenge(parameters, openChallenges)
            else -> return null
        }
        if (challenge != null && !challenge.isSolvable(parameters)) {
            return null
        }
        return challenge
    }

    fun generateClassCoverageChallenge(
        parameters: GitLabParameters, openChallenges: List<GChallenge>,
        forClass: Pair<String, String>? = null
    ): GChallenge? {
        val rndClass = forClass ?: JacocoInteraction.selectRandomClass(parameters)

        if (openChallenges
                .filterIsInstance<GClassCoverageChallenge>()
                .any { it.classPackage == rndClass.first && it.className == rndClass.second }
        ) return null

        val file = JacocoInteraction.getJacocoFile(rndClass.first, rndClass.second, parameters)
        if (file == null) {
            System.err.println("Could not get jacoco file for class ${rndClass.first}.${rndClass.second}")
            return null
        }
        val coverage = JacocoInteraction.parseClassCoverageInformation(file, rndClass.first, rndClass.second, parameters)

        val challenge = GClassCoverageChallenge(
            System.currentTimeMillis(),
            UUID.randomUUID().toString(),
            rndClass.first,
            rndClass.second,
            coverage
        )
        return challenge
    }

    fun generateMethodCoverageChallenge(
        parameters: GitLabParameters, openChallenges: List<GChallenge>,
        forClass: Pair<String, String>? = null): GChallenge? {
        val rndClass = forClass ?: JacocoInteraction.selectRandomClass(parameters)
        val rndMethod = JacocoInteraction.selectRandomMethod(rndClass.first, rndClass.second, parameters)

        if(rndMethod == null) {
            System.err.println("Class ${rndClass.first}.${rndClass.second} has no methods")
            return null
        }
        if (openChallenges
                .filterIsInstance<GMethodCoverageChallenge>()
                .any { it.classPackage == rndClass.first && it.className == rndClass.second && it.methodName == rndMethod }
        ) return null

        val file = JacocoInteraction.getJacocoFile(rndClass.first, rndClass.second, parameters)
        if (file == null) {
            System.err.println("[Gamekins] Could not get jacoco file for class ${rndClass.first}.${rndClass.second}")
            return null
        }

        val classCoverage = JacocoInteraction.parseClassCoverageInformation(file, rndClass.first, rndClass.second, parameters)
        val methodCoverage = JacocoInteraction.parseMethodCoverageInformation(file, rndClass.first, rndClass.second, rndMethod, parameters)
            ?: return null

        val challenge = GMethodCoverageChallenge(
            System.currentTimeMillis(),
            UUID.randomUUID().toString(),
            rndClass.first,
            rndClass.second,
            rndMethod,
            classCoverage,
            methodCoverage
        )
        return challenge
    }

    fun generateLineCoverageChallenge(
        parameters: GitLabParameters, openChallenges: List<GChallenge>,
        forClass: Pair<String, String>? = null): GChallenge? {
        val rndClass = forClass ?: JacocoInteraction.selectRandomClass(parameters)
        val rndLine = JacocoInteraction.selectRandomLine(rndClass.first, rndClass.second, parameters)

        if(rndLine == null) {
            System.err.println("[Gamekins] Class ${rndClass.first}.${rndClass.second} has no uncovered lines")
            return null
        }
        if (openChallenges
                .filterIsInstance<GLineCoverageChallenge>()
                .any { it.classPackage == rndClass.first && it.className == rndClass.second && it.lineNumber == rndLine }
        ) return null

        val file = JacocoInteraction.getJacocoFile(rndClass.first, rndClass.second, parameters)
        val sourceFile = JacocoInteraction.getJacocoSourceFile(rndClass.first, rndClass.second, parameters)
        if (file == null || sourceFile == null) {
            System.err.println("[Gamekins] Could not get jacoco file for class ${rndClass.first}.${rndClass.second}")
            return null
        }

        val classCoverage = JacocoInteraction.parseClassCoverageInformation(file, rndClass.first, rndClass.second, parameters)
        val lineCoverage = JacocoInteraction.parseLineCoverageInformation(sourceFile, rndClass.first, rndClass.second, rndLine, parameters)
            ?: return null

        val challenge = GLineCoverageChallenge(
            System.currentTimeMillis(),
            UUID.randomUUID().toString(),
            rndClass.first,
            rndClass.second,
            rndLine.toInt(),
            classCoverage,
            lineCoverage
        )
        return challenge
    }

    /**
     * Generates a new [BuildChallenge], puts it in store if the [parameters.isBuildSuccessful] was not ["success"]
     */
    fun generateBuildChallenge(
        openChallenges: List<GChallenge>
    ): GBuildChallenge? {
        // Only create challenge if it does not exist
        val buildChallengeExists = openChallenges.any { it is GBuildChallenge }

        if (buildChallengeExists) {
            return null
        }
        println("[Gamekins] Build failed! Generating a Build Failed Challenge!")

        // Create Challenge
        val buildChallenge = GBuildChallenge(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString()
        )

        return buildChallenge
    }

    fun generateTestCountChallenge(
        parameters: GitLabParameters, openChallenges: List<GChallenge> = emptyList(), testCount: Int? = null
    ): GChallenge? {
        // Only create test challenge if it does not exist
        val testChallengeExists = openChallenges.any { it is GTestCountChallenge }

        if (testChallengeExists) {
            return null
        }

        val initial: Int = testCount ?: try {
            JUnitInteraction.calcExecutedTestsFromJUnitXml(parameters)
        } catch (e: java.lang.Exception) {
            System.err.println("[Gamekins] Problem accessing JUnit Test files: ${e.message}")
            return null
        }
        val challenge = GTestCountChallenge(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            initialTestCount = initial
        )
        return challenge
    }

    fun generateSmellChallenge(parameters: GitLabParameters, openChallenges: List<GChallenge>,
        forClass: Pair<String, String>? = null): GChallenge? {
        // Get code smell
        val rndClass = forClass ?: JacocoInteraction.selectRandomClass(parameters)
        val smells = SonarLintInteraction.getSmellsOfFile(rndClass.first, rndClass.second, parameters)
        if(smells.isEmpty()) {
            System.err.println("[Gamekins] No code smells for class ${rndClass.first}.${rndClass.second}")
            return null
        }
        val smell = smells.random()

        // Generate Smell Challenge
        val lineContent = SourceFileInteraction.getLineContent(rndClass.first, rndClass.second, parameters, smell.startLine, smell.endLine)
        smell.lineContent = lineContent

        val file = JacocoInteraction.getJacocoSourceFile(rndClass.first, rndClass.second, parameters)
        if (file == null) {
            System.err.println("[Gamekins] Could not get jacoco file for class ${rndClass.first}.${rndClass.second}")
            return null
        }
        val line = smell.startLine ?: 1
        val coverage = JacocoInteraction.parseLineCoverageInformation(file, rndClass.first, rndClass.second, line, parameters)

        if (openChallenges
                .filterIsInstance<GSmellChallenge>()
                .any { it.codeSmell == smell}
        ) return null

        val challenge = GSmellChallenge(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            classPackage = rndClass.first,
            className = rndClass.second,
            codeSmell = smell,
            initialCoverage = coverage!!
        )
        return challenge
    }
}