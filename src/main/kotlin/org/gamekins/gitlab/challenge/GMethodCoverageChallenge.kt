package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.CoverageInformation
import org.gamekins.gitlab.interaction.JacocoInteraction
import java.text.NumberFormat
import java.util.Objects

data class GMethodCoverageChallenge(
    override val created: Long,
    override var id: String,
    val classPackage: String,
    val className: String,
    val methodName: String,
    val initialClassCoverage: CoverageInformation,
    val initialMethodCoverage: CoverageInformation
    ) : GChallenge {

    override val title: String
        get() = "\uD83E\uDDE9 Increase method coverage of $classPackage.$className.$methodName for ${getScorePointsStr()}!"

    override val description: String
        get() =
            "### Increase code coverage of $classPackage.$className.$methodName! \n\n" +
                    "**Increase coverage for ${getScorePointsStr()}!** \n\n" +
                    "```java\n//line ${initialMethodCoverage.startLine}\n${initialMethodCoverage.sourceCode}\n```\n\n" +
                    "**Current Class Coverage:** ${NumberFormat.getPercentInstance().format(initialClassCoverage.coverage)}\n\n" +
                    "**Current Method Coverage:** ${NumberFormat.getPercentInstance().format(initialMethodCoverage.coverage)}\n\n" +
                    "See the [Coverage Report](${initialMethodCoverage.reportUrl})"

    /**
     * Unsolvable if Jacoco File does not exist or every line is covered
     */
    override fun isSolvable(parameters: GitLabParameters): Boolean {
        val jacocoFile = JacocoInteraction.getJacocoFile(classPackage, className, parameters) ?: return false
        val cov = JacocoInteraction.parseMethodCoverageInformation(jacocoFile, classPackage, className, methodName, parameters) ?: return false
        return cov.missed > 0 && cov.coverage < 1
    }

    /**
     * The challenge is solved if the coverage percentage has increased
     */
    override fun isSolved(parameters: GitLabParameters): Boolean {
        val jacocoFile = JacocoInteraction.getJacocoFile(classPackage, className, parameters) ?: return false
        val cov = JacocoInteraction.parseMethodCoverageInformation(jacocoFile, classPackage, className, methodName, parameters) ?: return false

        return cov.coverage > initialMethodCoverage.coverage

    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is GMethodCoverageChallenge) return false
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }
    @JsonIgnore
    override fun getScore(): Int {
        return if(initialClassCoverage.coverage > 0.8) 2 else 1
    }

    override fun toString(): String {
        return title
    }
}