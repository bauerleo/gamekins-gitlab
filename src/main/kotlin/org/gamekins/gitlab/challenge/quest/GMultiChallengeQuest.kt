package org.gamekins.gitlab.challenge.quest

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.challenge.GChallenge
import org.gamekins.gitlab.challenge.GQuest
import org.gamekins.gitlab.GitLabParameters
import java.util.*

/**
 * Quest challenges are multi part challenges that contain multiple single challenges.
 *
 * If all sub challenges are solved by the same person, extra points are awarded!
 */
data class GMultiChallengeQuest(
    override val created: Long,
    override var id: String,
    override var assigneeId: String? = null,
    var customTitle: String = "",
    val subChallenges: List<InnerGChallenge> = emptyList(),
    ) : GQuest {

    override val title: String
        get() = customTitle

    override val description: String
        get() {
            val newCurrent = subChallenges.firstOrNull {!it.solved} ?: subChallenges.last()
            val subScore = subChallenges.map { it.c }.sumOf { it.getScore() }
            val stepsStr = subChallenges.joinToString(separator = "\n\n") {
                if(it.solved) "- [x] ${it.c}" else "- [ ] ${it.c}" }

            return  "Quests are individualized, whoever solves the first quest step, " +
                    "has the chance to win the extra Points by solving all quest steps!\n\n" +
                    "**Award: $subScore (+ ${subChallenges.size}) = ${getScore()} Points** \n\n" +
                    "You can only solve one quest step per run!\n\n\n\n" +
                    "### Current step: ${newCurrent.c} \n\n" +
                    "${newCurrent.c.description}\n\n" +
                    "### Quest steps: \n\n$stepsStr"
        }

    private val currentStep = subChallenges.firstOrNull {!it.solved} ?: subChallenges.last()
    override fun isSolvable(parameters: GitLabParameters): Boolean {
        return subChallenges
            .filter { !it.solved }
            .map { it.c }
            .all { it.isSolvable(parameters) }
    }

    /**
     * Contained Challenges must be solved one after another.
     * Check the challenge with the current index and increment the index.
     * The quest is solved if all contained challenges are solved.
     *
     * Contained challenges can only be solved step by step!
     *
     * Returns if all sub challenges are solved by the user with the current commit.
     */
    override fun isSolved(parameters: GitLabParameters): Boolean {
        if(!currentStep.solved) {
            currentStep.solved = stepIsSolved(parameters)
        }
        return subChallenges.all { it.solved }
    }

    @get:JsonIgnore
    override val currentChallengeIndex: Int
        get() = subChallenges.indexOf(currentStep)

    override fun stepIsSolved(parameters: GitLabParameters): Boolean {
        // Quests can only be solved by assignee
        if(assigneeId != null && parameters.committerId != assigneeId) {
            return false;
        }

        // Check if current step is solved
        val solved = currentStep.c.isSolved(parameters)
        if(assigneeId == null && solved) {
            this.assigneeId = parameters.committerId
        }

        return solved
    }

    @JsonIgnore
    override fun getScore(): Int {
        return subChallenges.map { it.c }.sumOf { it.getScore() } + subChallenges.size
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is GQuest) return false
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }

    override fun toString(): String {
        return title
    }

    data class InnerGChallenge(val c: GChallenge, var solved: Boolean = false)
}
