package org.gamekins.gitlab.challenge.quest

import org.gamekins.gitlab.challenge.*
import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.JUnitInteraction
import org.gamekins.gitlab.interaction.JacocoInteraction
import java.util.UUID

object GQuestFactory {

    fun generateNQuests(n: Int, parameters: GitLabParameters, openChallenges: List<GChallenge>): List<GQuest> {
        val result = mutableListOf<GQuest>()

        var tries = 20

        while (tries > 0 && result.size < n) {
            val quest =
                when((0..6).random()) {
                    0 -> generateTestQuest(parameters, openChallenges + result)
                    1 -> generateClassQuest(parameters)
                    2 -> generateMethodQuest(parameters)
                    3 -> generateLineQuest(parameters)
                    4 -> generateExpandingQuest(parameters)
                    5 -> generateDecreasingQuest(parameters)
                    6 -> generateSmellQuest(parameters)
                    else -> null
                }
            if(quest != null) {
                result.add(quest)
            }
            tries--
        }
        return result
    }

    fun generateTestQuest(parameters: GitLabParameters, openChallenges: List<GChallenge>): GQuest? {
        // Only create test quest if it does not exist already
        val hasTestQuest = openChallenges
            .filterIsInstance<GMultiChallengeQuest>()
            .flatMap { it.subChallenges}
            .any { it.c is GTestCountChallenge }

        if(hasTestQuest) {
            return null
        }

        var testCount =
        try {
            JUnitInteraction.calcExecutedTestsFromJUnitXml(parameters)
        } catch (e: java.lang.Exception) {
            System.err.println("[Gamekins] Problem accessing JUnit Test files: ${e.message}")
            return null
        }

        // Create test
        val sub = ArrayList<GMultiChallengeQuest.InnerGChallenge>()
        repeat(3) {
            val c = GChallengeFactory.generateTestCountChallenge(parameters, testCount = testCount)
            if(c != null) {
                sub.add(GMultiChallengeQuest.InnerGChallenge(c))
                testCount += 1
            }
        }
        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = sub,
            customTitle = "\uD83E\uDDEA Solve all test challenges one after another for ${sub.size} extra Points!"
        )
        return quest
    }

    private fun generateClassQuest(parameters: GitLabParameters): GQuest? {
        val challenges = mutableListOf<GMultiChallengeQuest.InnerGChallenge>()
        val rndClass = JacocoInteraction.selectRandomClass(parameters)

        repeat(3) {
            val c = GChallengeFactory.generateClassCoverageChallenge(parameters, emptyList(), rndClass) ?: return null
            challenges.add(GMultiChallengeQuest.InnerGChallenge(c))
        }
        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = challenges,
            customTitle = "\uD83D\uDCD6 Solve all class coverage challenges one after another for ${challenges.size} extra Points!"
        )
        return quest
    }

    private fun generateMethodQuest(parameters: GitLabParameters): GQuest? {
        val challenges = mutableListOf<GMultiChallengeQuest.InnerGChallenge>()
        val rndClass = JacocoInteraction.selectRandomClass(parameters)

        repeat(3) {
            val c = GChallengeFactory.generateMethodCoverageChallenge(parameters, challenges.map { it.c }, rndClass) ?: return null
            challenges.add(GMultiChallengeQuest.InnerGChallenge(c))
        }

        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = challenges,
            customTitle = "\uD83E\uDDE9 Solve all method coverage challenges in ${rndClass.first}.${rndClass.second} for ${challenges.size} extra Points!"
        )
        return quest
    }

    private fun generateLineQuest(parameters: GitLabParameters): GQuest? {
        val challenges = mutableListOf<GMultiChallengeQuest.InnerGChallenge>()
        val rndClass = JacocoInteraction.selectRandomClass(parameters)

        repeat(3) {
            val c = GChallengeFactory.generateLineCoverageChallenge(parameters, emptyList(), rndClass) ?: return null
            challenges.add(GMultiChallengeQuest.InnerGChallenge(c))
        }

        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = challenges,
            customTitle = "✏️ Solve all line coverage challenges in ${rndClass.first}.${rndClass.second} for ${challenges.size} extra Points!"
        )
        return quest
    }

    private fun generateExpandingQuest(parameters: GitLabParameters): GQuest? {
        val rndClass = JacocoInteraction.selectRandomClass(parameters)
        val challenges = expandingChallenges(parameters, rndClass) ?: return null

        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = challenges,
            customTitle = "\uD83D\uDCF6️ Solve all expanding challenges in ${rndClass.first}.${rndClass.second} for ${challenges.size} extra Points!"
        )
        return quest
    }

    private fun generateDecreasingQuest(parameters: GitLabParameters): GQuest? {
        val rndClass = JacocoInteraction.selectRandomClass(parameters)
        val challenges = expandingChallenges(parameters, rndClass) ?: return null

        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = challenges.reversed(),
            customTitle = "⏚  Solve all decreasing challenges in ${rndClass.first}.${rndClass.second} for ${challenges.size} extra Points!"
        )
        return quest
    }

    private fun expandingChallenges(parameters: GitLabParameters, rndClass: Pair<String, String>): List<GMultiChallengeQuest.InnerGChallenge>? {
        val line = GChallengeFactory.generateLineCoverageChallenge(parameters, emptyList(), rndClass) ?: return null
        val method = GChallengeFactory.generateMethodCoverageChallenge(parameters, emptyList(), rndClass) ?: return null
        val clazz = GChallengeFactory.generateClassCoverageChallenge(parameters, emptyList(), rndClass) ?: return null
        return listOf(
            GMultiChallengeQuest.InnerGChallenge(line),
            GMultiChallengeQuest.InnerGChallenge(method),
            GMultiChallengeQuest.InnerGChallenge(clazz))
    }

    private fun generateSmellQuest(parameters: GitLabParameters): GQuest? {
        val challenges = mutableListOf<GMultiChallengeQuest.InnerGChallenge>()
        val rndClass = JacocoInteraction.selectRandomClass(parameters)

        repeat(3) {
            val c = GChallengeFactory.generateSmellChallenge(parameters, challenges.map { it.c }, rndClass) ?: return null
            challenges.add(GMultiChallengeQuest.InnerGChallenge(c))
        }

        val quest = GMultiChallengeQuest(
            created = System.currentTimeMillis(),
            id = UUID.randomUUID().toString(),
            subChallenges = challenges,
            customTitle = "\uD83D\uDCA9 The smell is really bad! Solve all smell challenges in ${rndClass.first}.${rndClass.second} for ${challenges.size} extra Points!"
        )
        return quest
    }
}