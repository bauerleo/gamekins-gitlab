package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.CoverageInformation
import org.gamekins.gitlab.interaction.SonarIssue
import org.gamekins.gitlab.interaction.SonarLintInteraction
import org.gamekins.gitlab.interaction.javaFolder
import java.nio.file.Paths

data class GSmellChallenge(
    override val created: Long,
    override var id: String,
    val classPackage: String,
    val className: String,
    val codeSmell: SonarIssue,
    val initialCoverage: CoverageInformation) : GChallenge {

    override val title: String
        get() = "\uD83D\uDC7E Fix code smell in $classPackage.$className for ${getScorePointsStr()}!"
    override val description: String
        get() = calcDescription()

    private fun calcDescription(): String {
        val line = if (codeSmell.startLine == null) "concerning the whole class $classPackage.$className"
            else "in line ${codeSmell.startLine}-${codeSmell.endLine}"
        val start = if(codeSmell.startLine == null) ""
            else "//line ${codeSmell.startLine}-${codeSmell.endLine}\n"
        val tip = codeSmell.message


        return "### Fix code smell from SonarLint $line to earn ${getScorePointsStr()}!\n\n" +
                "**Severity: ${codeSmell.severity}** \n\n" +
                "**Description:**\n\n" +
                "```\n$tip\n```\n\n" +
                "**Source Code**: \n\n" +
                "```java\n$start ${codeSmell.lineContent}\n```\n\n" +
                "See the [Coverage Report](${initialCoverage.reportUrl}.) \n\n" +
                "Checkout the [SonarLint Rule](${codeSmell.url})"
    }

    override fun isSolvable(parameters: GitLabParameters): Boolean {
        val issues = SonarLintInteraction.getSmellsOfFile(classPackage, className, parameters)

        return issues.any {
            it == codeSmell
        }
    }

    override fun isSolved(parameters: GitLabParameters): Boolean {
        val path = Paths.get(
            parameters.basePath, javaFolder, classPackage.replace(".", "/"), "${className}.java"
        )
        if(!path.toFile().exists()) {
            return false
        }
        val issues = SonarLintInteraction.getSmellsOfFile(classPackage, className, parameters)
        return issues.none {
            it == codeSmell
        }
    }

    @JsonIgnore
    override fun getScore(): Int {
        return when (codeSmell.severity) {
            "BLOCKER" -> 4
            "CRITICAL" -> 3
            "MAJOR" -> 2
            else -> 1
        }
    }
}