package org.gamekins.gitlab.challenge

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.gamekins.gitlab.interaction.CoverageInformation
import org.gamekins.gitlab.interaction.JacocoInteraction
import java.text.NumberFormat
import java.util.Objects

data class GClassCoverageChallenge(
    override val created: Long,
    override var id: String,
    val classPackage: String,
    val className: String,
    val initialCoverage: CoverageInformation
    ) : GChallenge {

    override val title: String
        get() = "\uD83D\uDCD6 Increase class coverage of $classPackage.$className for ${getScorePointsStr()}!"

    override val description: String
        get() =
            "### Increase code coverage of $classPackage.$className! \n\n" +
                    "**Increase coverage for ${getScorePointsStr()}!** \n\n" +
                    "**Current Coverage:** ${NumberFormat.getPercentInstance().format(initialCoverage.coverage)}\n\n" +
                    "<details>\n" +
                    "<summary>Class source code</summary>\n\n" +
                    "```java\n${initialCoverage.sourceCode}\n```\n\n" +
                    "</details>\n\n" +
                    "See the [Coverage Report](${initialCoverage.reportUrl})"

    /**
     * Unsolvable if Jacoco File does not exist or every line is covered
     */
    override fun isSolvable(parameters: GitLabParameters): Boolean {
        val jacocoFile = JacocoInteraction.getJacocoFile(classPackage, className, parameters) ?: return false
        val cov = JacocoInteraction.parseClassCoverageInformation(jacocoFile, classPackage, className, parameters)
        return cov.missed > 0 && cov.coverage < 1
    }

    /**
     * The challenge is solved if the coverage percentage has increased
     */
    override fun isSolved(parameters: GitLabParameters): Boolean {
        val jacocoFile = JacocoInteraction.getJacocoFile(classPackage, className, parameters) ?: return false
        val cov = JacocoInteraction.parseClassCoverageInformation(jacocoFile, classPackage, className, parameters)

        return cov.coverage > initialCoverage.coverage

    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false
        if (other !is GClassCoverageChallenge) return false
        return Objects.equals(this.id, other.id)
    }

    override fun hashCode(): Int {
        return Objects.hashCode(id)
    }
    @JsonIgnore
    override fun getScore(): Int {
        return if(initialCoverage.coverage > 0.8) 2 else 1
    }

    override fun toString(): String {
        return title
    }
}