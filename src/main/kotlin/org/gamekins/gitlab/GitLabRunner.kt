package org.gamekins.gitlab

import org.apache.commons.cli.CommandLineParser
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import org.gitlab4j.api.GitLabApi
import java.nio.file.Paths
import kotlin.system.exitProcess


fun main(args: Array<String>) {
    val parameters = parseParameters(args)
    validateParameters(parameters)

    val gitLabApi = GitLabApi(parameters.gitLabUrl, parameters.accessToken)
    val gamekins = GitLabGamekins(gitLabApi, parameters.projectId, parameters)
    gamekins.execute()
    exitProcess(0)
}

fun parseParameters(args: Array<String>): GitLabParameters {
    val parser: CommandLineParser = DefaultParser()

    val options = Options()
    options.addOption("s","build-success", true, "Build Exit code")
    options.addOption("u","api-user-partof-leaderboard", false, "Should the user with the access token be part of the leaderboard?")

    val line = parser.parse(options, args)

    val params = GitLabParameters(
        basePath = System.getenv("BASE_PATH") ?: ".",
        classesPath = System.getenv("CLASSES_FOLDER") ?: "target/classes",
        jacocoResultsPath = System.getenv("JACOCO_RESULTS_PATH") ?: "target/site/jacoco",
        junitResultsPath = System.getenv("JUNIT_RESULTS_PATH") ?: "target/surefire-reports/",
        gitLabUrl = System.getenv("CI_SERVER_URL") ?: "https://gitlab.com",
        accessToken = System.getenv("GITLAB_ACCESS_TOKEN"),
        projectId = System.getenv("CI_PROJECT_ID"),
        jobUrl = System.getenv("CI_JOB_URL"),
        projectUrl = System.getenv("CI_PROJECT_URL"),
        isBuildSuccessful = line.getOptionValue("s") == "success",
        apiUserPartofLeaderboard = line.hasOption("api-user-partof-leaderboard"),
        committerId = ""
    )
    return params
}

fun validateParameters(parameters: GitLabParameters) {
    //Checks whether the paths of the JaCoCo files are correct
    val jacocoResults = Paths.get(
        parameters.basePath,
        parameters.jacocoResultsPath
    )
    if (!jacocoResults.toFile().exists()) {
        System.err.println("[Gamekins] JaCoCo folder is not correct")
    }
    if (!Paths.get(jacocoResults.toString(), "jacoco.csv" ).toFile().exists()) {
        System.err.println("[Gamekins] JaCoCo csv file could not be found")
        return
    }
}