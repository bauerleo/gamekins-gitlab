package org.gamekins.gitlab.gitlab

import org.gamekins.gitlab.challenge.GChallenge
import org.gamekins.gitlab.challenge.GQuest
import org.gamekins.gitlab.GitLabParameters
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.Issue
import java.util.*

object GitLabIssue {
    /**
     * Creates an issue for a challenge and returns it
     */
    fun createIssue(c: GChallenge, parameters: GitLabParameters, label: String): Issue {
        val preTitle = if(c is GQuest) "Quest: " else "Challenge: "

        val issue = parameters.gitLabApi.issuesApi.createIssue(
            parameters.projectId,
            "$preTitle ${c.title}",
            c.description,
            false,
            null,
            null,
            label,
            Date(),
            null,
            null,
            null
        )
        return issue
    }

    fun closeIssueForSolved(
        c: GChallenge,
        solvedBy: GitLabUser,
        commitId: String,
        parameters: GitLabParameters
    ) {
        println("[Gamekins] Close solved issue #${c.id}")

        // Assign solved issue to solver
        if(solvedBy.isProjectMember) {
            parameters.gitLabApi.issuesApi.updateIssue(
                parameters.projectId,
                c.id.toLong(),
                null,
                null,
                null,
                if(solvedBy.id > 0) listOf(solvedBy.id) else null,
                null,
                null,
                null,
                Date(),
                null
            )
        }

        val leaderboardUrl = "${parameters.projectUrl}/wikis/${WIKI_LEADERBOARD_SLUG}"
        val usrName = if(solvedBy.isProjectMember) "([${solvedBy.userName}](${solvedBy.webUrl}))" else ""
        val comment =
            "\uD83C\uDF89 \n\n ${solvedBy.name} $usrName solved this issue with commit $commitId in [job](${parameters.jobUrl}) and gains ${c.getScore()} point(s)! \n\n \uD83C\uDF1F Congratulations! \uD83C\uDF1F \n\n Checkout the [Leaderboard]($leaderboardUrl) in the Wiki!"
        closeIssue(c.id, comment, parameters)
    }

    fun closeIssueForUnsolvable(c: GChallenge, commitId: String, parameters: GitLabParameters) {
        println("[Gamekins] Close unsolved issue #${c.id}")

        val comment = "Challenge ${c.title} (#${c.id}) is not solvable anymore after commit $commitId"
        closeIssue(c.id, comment, parameters)

    }

    fun commentIssueForRejected(c: GChallenge, parameters: GitLabParameters) {
        println("[Gamekins] Comment rejected issue #${c.id}")

        val comment = "Challenge ${c.title} (#${c.id}) has been rejected by manual closing of the issue."
        closeIssue(c.id, comment, parameters)

    }

    fun closeIssue(issueId: String, comment: String, parameters: GitLabParameters) {

        try {
            // Create closing comment
            parameters.gitLabApi.notesApi.createIssueNote(parameters.projectId, issueId.toLong(), comment)

            // Close issue
            parameters.gitLabApi.issuesApi.closeIssue(parameters.projectId, issueId.toLong())
        } catch (e: GitLabApiException){
            System.err.println("[Gamekins] Could not close issue: ${e.message}")
        }
    }

    fun updateIssueForSolvedStep(q: GQuest, solvedBy: GitLabUser, commitId: String, parameters: GitLabParameters, label: String) {
        val comment = "\uD83C\uDF89 \n\n ${solvedBy.name} ([${solvedBy.userName}](${solvedBy.webUrl})) solved Step ${q.currentChallengeIndex } with commit $commitId in [job](${parameters.jobUrl})! \n\n \uD83C\uDF1F Congratulations! \uD83C\uDF1F \n"
        val assignee = if(parameters.committerId.toLong() > 0) listOf(parameters.committerId.toLong()) else emptyList()

        // Create assignee comment
        parameters.gitLabApi.notesApi.createIssueNote(parameters.projectId, q.id.toLong(), comment)

        // Update description and assignee
        parameters.gitLabApi.issuesApi.updateIssue(
            parameters.projectId,
            q.id.toLong(),
            null,
            q.description,
            false,
            assignee,
            null,
            label,
            null,
            Date(),
            null
        )
    }
}