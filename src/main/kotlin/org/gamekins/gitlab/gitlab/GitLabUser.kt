package org.gamekins.gitlab.gitlab

import org.gitlab4j.api.GitLabApi
import java.util.*
import kotlin.collections.ArrayList

fun getAllUsers(api: GitLabApi, project: String): List<GitLabUser> {
    println("[Gamekins] Getting all project Members")

    val users = api.projectApi.getAllMembers(project).map {
        GitLabUser(
            avatarUrl = it.avatarUrl,
            createdAt = it.createdAt,
            email = it.email,
            id = it.id,
            name = it.name,
            state = it.state,
            userName = it.username,
            webUrl = it.webUrl,
            isProjectMember = true
        )
    }.toList()
    return users
}

fun getMe(api: GitLabApi): GitLabUser {
    val response = api.userApi.currentUser
    return GitLabUser(
        avatarUrl = response.avatarUrl,
        createdAt = response.createdAt,
        email = response.email,
        id = response.id,
        name = response.name,
        state = response.state,
        userName = response.username,
        webUrl = response.webUrl,
        isProjectMember = true
    )
}

data class GitLabUser(
    val id: Long,
    val name: String,
    val email: String?,
    val userName: String,
    val score: Int = 0,
    val isProjectMember: Boolean = false,
    val solvedChallengeIds: MutableList<String> = ArrayList(),
    val avatarUrl: String,
    val createdAt: Date,
    val state: String,
    val webUrl: String,
) {

    /**
     * Equals ony checks id property for project members, els name is used
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GitLabUser) return false

        if(isProjectMember) {
            return this.id == other.id
        }
        return this.name == other.name
    }

    /**
     * Hashcode only checks id property
     */
    override fun hashCode(): Int {
        if(isProjectMember) {
            return Objects.hashCode(this.id)
        }
        return Objects.hashCode(this.name)
    }
}