package org.gamekins.gitlab.gitlab

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.gamekins.gitlab.GitLabParameters
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.WikiPage

const val WIKI_LEADERBOARD_SLUG = "Gamekins-Leaderboard"

object GLeaderboard {

    fun updateLeaderBoard(store: GStoreContent, parameters: GitLabParameters) {
        println("[Gamekins] Updating the Leaderboard")

        val content = createLeaderBoardContent(store.users, store)

        // Delete Leaderboard page
        try {
            parameters.gitLabApi.wikisApi.deletePage(parameters.projectId, WIKI_LEADERBOARD_SLUG)
        } catch (e: GitLabApiException) {
            // Page does not exist
        }

        // Save Leaderboard to wiki
        try {
            parameters.gitLabApi.wikisApi.createPage(parameters.projectId, WIKI_LEADERBOARD_SLUG, content)
        } catch (e: GitLabApiException) {
            System.err.println("[Gamekins] Could not create store wiki page ${e.message}")
        }

    }

    private fun createLeaderBoardContent(users: Collection<GitLabUser>, store: GStoreContent): String {
        val result = StringBuilder()
        val sortedUsers = users.sortedByDescending { it.score }
        val storeJson = jacksonObjectMapper().writeValueAsString(store)


        result.append("Open Challenges: ${store.openChallenges.joinToString { "#${it.id}"}}")
        result.append("\n\n\n")
        result.append("|AVATAR|Full NAME|USER NAME|PROJECT MEMBER|SCORE|SOLVED CHALLENGES| \n")
        result.append("|------|:-------:|:-------:|:------------:|:---:|-----------------| \n")

        sortedUsers.map {
            val isMember = if(it.isProjectMember) "YES" else "NO"
            "|![Avatar](${it.avatarUrl})|**${it.name}**|**[${it.userName}](${it.webUrl})**|**$isMember**|**${it.score}**|${it.solvedChallengeIds.joinToString { "#$it" }} \n"
        }.forEach{result.append(it)}

        // Persist store as html comment
        result.append("\n\n\n\n\n")
        result.append("<!-- $storeJson -->")
        return result.toString()
    }

    fun getStoreFromWiki(api: GitLabApi, parameters: GitLabParameters): GStoreContent {
        println("[Gamekins] Get Gamekins internal state from GitLab wiki")
        val page: WikiPage
        try {
            page = api.wikisApi.getPage(parameters.projectId, WIKI_LEADERBOARD_SLUG)
        } catch (e: GitLabApiException) {
            // Page does not exist, return empty
            System.err.println("[Gamekins] Could not fetch internal Wiki page: ${e.message}")
            return GStoreContent()
        }

        val matchResult = Regex("<!--\\s(.*)\\s-->").find(page.content) ?: return GStoreContent()
        val (json) = matchResult.destructured

        // serialize to class
        val mapper = jacksonObjectMapper()
        val storeContent = mapper.readValue(json, GStoreContent::class.java)
        return storeContent
    }
}
