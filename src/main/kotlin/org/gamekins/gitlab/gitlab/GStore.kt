package org.gamekins.gitlab.gitlab

import org.gamekins.gitlab.challenge.GChallenge
import org.gamekins.gitlab.GitLabParameters
import org.gitlab4j.api.GitLabApi

object GStore {

    var content: GStoreContent = GStoreContent()

    fun initializeStore(api: GitLabApi, parameters: GitLabParameters): GStore {
        val response = GLeaderboard.getStoreFromWiki(api, parameters)
        this.content = response
        return this
    }
}

data class GStoreContent(
    val openChallenges: MutableList<GChallenge> = ArrayList(),
    val users: MutableSet<GitLabUser> = HashSet(),
)