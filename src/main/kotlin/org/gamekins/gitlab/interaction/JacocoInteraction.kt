package org.gamekins.gitlab.interaction

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.util.stream.Collectors

const val classCoverageXPath = "/html/body/table/tfoot/tr/td[2]"
const val methodTableCoverageXPath = "/html/body/table/tbody"

object JacocoInteraction {
    fun getJacocoFile(
        classPackage: String, className: String, parameters: GitLabParameters, fileType: String = ".html"
    ): File? {
        val path = Paths.get(
            parameters.basePath, parameters.jacocoResultsPath, classPackage, "${className}$fileType"
        )
        if (!Files.exists(path)) {
            System.err.println("[Gamekins] Jacoco coverage for class ${classPackage}.$className does not exist!")
            return null
        }
        return path.toFile()
    }

    fun getJacocoSourceFile(classPackage: String, className: String, parameters: GitLabParameters) =
        getJacocoFile(classPackage, className, parameters, fileType = ".java.html")

    fun parseClassCoverageInformation(
        jacocoFile: File, classPackage: String, className: String, parameters: GitLabParameters
    ): CoverageInformation {
        val doc = Jsoup.parse(jacocoFile)
        val element = doc.selectXpath(classCoverageXPath)[0]
        val cov = element.text().split(" of ").map { it.toInt() }
        val reportUrl =
            "${parameters.jobUrl}/artifacts/file/${parameters.jacocoResultsPath}/$classPackage/$className.html"
        val sourceCode = SourceFileInteraction.getLineContent(classPackage, className, parameters, null, null)

        return CoverageInformation(missed = cov[0], all = cov[1],
            reportUrl = reportUrl, startLine = 1, sourceCode = sourceCode)
    }

    fun parseMethodCoverageInformation(
        jacocoFile: File, classPackage: String, className: String, methodName: String, parameters: GitLabParameters
    ): CoverageInformation? {
        val doc = Jsoup.parse(jacocoFile)
        val methods = doc.selectXpath(methodTableCoverageXPath)[0].children()
        val method = methods.find { it.children()[0].text() == methodName }

        if (method == null) {
            System.err.println("Could not find method $methodName for class $classPackage.$className")
            return null
        }
        val elements = method.children()

        val missed = elements[7].text().toInt()
        val all = elements[8].text().toInt()
        val href = elements[0].children()[0].attr("href")
        val reportUrl = "${parameters.jobUrl}/artifacts/file/${parameters.jacocoResultsPath}/$classPackage/$href"
        var start = href.split("#L").getOrNull(1)?.toIntOrNull()
        start = if(start == null || start < 6) 1 else start - 5
        val end =  if(start != null) start + 10 else null
        val sourceCode = SourceFileInteraction.getLineContent(classPackage, className, parameters, start, end)

        return CoverageInformation(missed = missed, all = all, reportUrl = reportUrl,
            startLine = start ?: 1, sourceCode = sourceCode)
    }

    fun parseLineCoverageInformation(
        jacocoSourceFile: File, classPackage: String, className: String, lineNumber: Int, parameters: GitLabParameters
    ): CoverageInformation? {
        val doc = Jsoup.parse(jacocoSourceFile)
        val line = doc.selectXpath("//*[@id=\"L$lineNumber\"]") ?: return null
        val isCovered = line.hasClass("fc");
        val missed = if(isCovered) 0 else 1
        val all = 1
        val reportUrl = "${parameters.jobUrl}/artifacts/file/${parameters.jacocoResultsPath}/$classPackage/$className.java.html#L$lineNumber"
        val sourceCode = SourceFileInteraction.getLineContent(classPackage, className, parameters, lineNumber, lineNumber)
        return CoverageInformation(missed = missed, all = all,
            startLine = lineNumber, reportUrl = reportUrl, sourceCode = sourceCode)
    }

    fun selectRandomClass(parameters: GitLabParameters): Pair<String, String> {
        // Select random class file in classes folder
        val classesPath = Paths.get(parameters.basePath, parameters.classesPath)

        val classes = Files.walk(classesPath).filter { it.toFile().isFile }.filter { it.toString().endsWith(".class") }
            .map { classesPath.relativize(it).normalize().toString() }.collect(Collectors.toList())
        val rndClass = classes.asSequence().shuffled().elementAt(0)

        // Split into package path and Class name
        val i = rndClass.lastIndexOf("/")
        val packageStr = rndClass.substring(0, i).replace("/", ".")
        val className = rndClass.substring(i + 1).replace(".class", "")
        return Pair(packageStr, className)
    }

    fun selectRandomMethod(classPackage: String, className: String, parameters: GitLabParameters): String? {
        val jacoco = getJacocoFile(classPackage, className, parameters) ?: return null

        val doc = Jsoup.parse(jacoco)
        val methods = doc.selectXpath(methodTableCoverageXPath)[0].children()
        if (methods.size == 0) return null

        val rnd = methods.shuffled().elementAt(0)
        return rnd.children()[0].children()[0].text()
    }

    fun selectRandomLine(classPackage: String, className: String, parameters: GitLabParameters): Int? {
        val jacocoSourceFile = getJacocoSourceFile(classPackage, className, parameters) ?: return null
        val doc = Jsoup.parse(jacocoSourceFile)

        // Select all partially covered (pc) and not covered (nc) lines that have instructions that can
        // actually be covered
        val elements = doc.select("span." + "pc")
        elements.addAll(doc.select("span." + "nc"))
        elements.removeIf { e: Element ->
            (e.text().trim() == "{" || e.text().trim() == "}" || e.text().trim() == "(" || e.text()
                .trim() == ")" || e.text().contains("class") || e.text().contains("void") || e.text()
                .contains("public") || e.text().contains("private") || e.text().contains("protected") || e.text()
                .contains("static"))
        }
        elements.removeIf { isGetterOrSetter(jacocoSourceFile.readText().split("\n"), it.text()) }

        if(elements.isEmpty()) {
            return null
        }
        val rnd = elements.shuffled().elementAt(0)

        // The linenumber is inside the id property in the jacoco report
        return rnd.attr("id").substring(1).toInt()
    }

    private fun isGetterOrSetter(lines: List<String>, line: String): Boolean {
        val linesIterator = lines.listIterator()
        while (linesIterator.hasNext()) {
            if (linesIterator.next().contains(line)) {
                while (linesIterator.hasPrevious()) {
                    val previous = linesIterator.previous()
                    if (!previous.contains(line) && previous.isNotBlank() && previous.trim() != "{") {
                        return checkMethodHeaderForGetterSetter(previous, line)
                    }
                }
            }
        }
        return false
    }

    private fun checkMethodHeaderForGetterSetter(previous: String, line: String): Boolean {
        val regex = Regex("[a-zA-Z]+ +(get|set|is)([a-zA-Z_]+)\\(.*\\)")
        val result = regex.find(previous)
        if (result != null) {
            val variable = result.groupValues[2]
            if (line.contains(variable, ignoreCase = true)) return true
        }
        return false
    }
}

data class CoverageInformation(
    val missed: Int, val all: Int, val reportUrl: String,

    @JsonIgnore
    val startLine: Int = 1,

    @JsonIgnore
    val sourceCode: String = ""
) {
    @JsonIgnore
    val coveredInstructions = all - missed

    @JsonIgnore
    val coverage = (coveredInstructions.toDouble() / all)

}
