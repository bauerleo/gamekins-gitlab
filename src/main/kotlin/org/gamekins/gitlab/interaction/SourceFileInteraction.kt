package org.gamekins.gitlab.interaction

import org.gamekins.gitlab.GitLabParameters
import java.nio.file.Paths

object SourceFileInteraction {

    fun getLineContent(classPackage: String, className: String, parameters: GitLabParameters, startLine: Int?, endLine: Int?): String {
        val path = Paths.get(
            parameters.basePath, javaFolder, classPackage.replace(".", "/"), "${className}.java"
        )
        val javaFile = path.toFile()

        if(!javaFile.exists()) {
            return ""
        }

        if (startLine == null || endLine == null) return javaFile.readText()
        return javaFile.readText()
            .split("\n")
            .filterIndexed { index, _ ->
                if (index < (startLine - 1)) {
                    false
                } else index <= (endLine - 1) }
            .reduce { acc, s -> "$acc\n$s" }
    }
}