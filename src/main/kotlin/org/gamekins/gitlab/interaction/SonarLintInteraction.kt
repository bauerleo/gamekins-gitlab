package org.gamekins.gitlab.interaction

import com.fasterxml.jackson.annotation.JsonIgnore
import org.gamekins.gitlab.GitLabParameters
import org.sonarsource.sonarlint.core.StandaloneSonarLintEngineImpl
import org.sonarsource.sonarlint.core.analysis.api.ClientInputFile
import org.sonarsource.sonarlint.core.client.api.common.analysis.Issue
import org.sonarsource.sonarlint.core.client.api.common.analysis.IssueListener
import org.sonarsource.sonarlint.core.client.api.standalone.StandaloneAnalysisConfiguration
import org.sonarsource.sonarlint.core.client.api.standalone.StandaloneGlobalConfiguration
import org.sonarsource.sonarlint.core.client.api.standalone.StandaloneSonarLintEngine
import org.sonarsource.sonarlint.core.commons.Language
import org.sonarsource.sonarlint.core.commons.log.ClientLogOutput
import java.io.FileInputStream
import java.io.InputStream
import java.net.URI
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Objects

const val javaFolder = "src/main/java"

object SonarLintInteraction {

    @JvmStatic
    fun getSmellsOfFile(
        classPackage: String, className: String, parameters: GitLabParameters
    ): List<SonarIssue> {
        val path = Paths.get(
            parameters.basePath, javaFolder, classPackage.replace(".", "/"), "${className}.java"
        )
        if(!path.toFile().exists()) {
            return emptyList()
        }

        // Build Sonar Configuration
        val javaFile = JavaSourceFile(path)
        val engine = getEngine()

        val analysisConfig = StandaloneAnalysisConfiguration.builder()
            .addInputFile(javaFile)
            .setBaseDir(Paths.get(parameters.basePath))
            .build()
        val issues = mutableListOf<Issue>()
        val issueListener = IssueListener { issue -> issues.add(issue) }
        val messages = mutableListOf<Pair<ClientLogOutput.Level, String>>()
        val logOutput = ClientLogOutput { msg, level -> messages.add(Pair(level, msg)) }

        // Analyze source code
        engine.analyze(analysisConfig, issueListener, logOutput, null)
        messages
            .filter { it.first == ClientLogOutput.Level.ERROR }
            .forEach { println("[Gamekins] SonarLint: ${it.second}")
        }
        return issues.map { SonarIssue(
            severity = it.severity,
            ruleKey = it.ruleKey,
            type = it.type,
            startLine = it.startLine,
            endLine = it.endLine,
            message = it.message,
        ) }
    }

    private var _engine: StandaloneSonarLintEngine? = null

    private fun getEngine(): StandaloneSonarLintEngine {
        if(_engine != null) {
            return _engine as StandaloneSonarLintEngine
        }

        val globalConfig = StandaloneGlobalConfiguration.builder()
            .addEnabledLanguage(Language.JAVA)
            .addPlugin(pathToSonarJavaPlugin())
            .build()
        _engine = StandaloneSonarLintEngineImpl(globalConfig)
        return _engine as StandaloneSonarLintEngine
    }

    /**
     * Returns the path to the jar file of the Sonar-Java-Plugin for SonarLint.
     */
    private fun pathToSonarJavaPlugin(): Path {
        // local development
        var libFolder = Path.of(
            System.getProperty("user.dir"),
            "target/lib"
        )

        // Inside docker container
        if(!libFolder.toFile().exists()) {
            libFolder = Path.of("/lib")
        }
        val jar = Files.list(libFolder)
            .filter{it.fileName.toString().startsWith("sonar-java-plugin")}
            .filter {it.fileName.toString().endsWith(".jar")}
            .findFirst()
        return jar.get()
    }

}

// Adapter classes

data class SonarIssue(
    val severity: String,
    val ruleKey: String,
    val type: String,
    val startLine: Int?,
    val endLine: Int?,
    val message: String?,
    var lineContent: String? = null,
) {
    @get:JsonIgnore
    val url: String
        get() = "https://rules.sonarsource.com/java/RSPEC-${ruleKey.split("S")[1]}"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SonarIssue
        return (severity == other.severity
                && ruleKey == other.ruleKey
                && type == other.type
                && ((startLine == other.startLine && endLine == other.endLine)
                    || lineContent == other.lineContent))
    }

    override fun hashCode(): Int {
        return Objects.hash(
            severity,
            ruleKey,
            type,
            startLine,
            endLine,
            message,
        )
    }
}

class JavaSourceFile(
    private val path: Path
): ClientInputFile {
    override fun getPath(): String {
        return path.toAbsolutePath().toString()
    }

    override fun isTest(): Boolean {
        return false
    }

    override fun getCharset(): Charset? {
        return null
    }

    override fun <G : Any?> getClientObject(): G? {
        return null;
    }

    override fun inputStream(): InputStream {
        return FileInputStream(path.toFile())
    }

    override fun contents(): String {
        return Files.readString(path)
    }

    override fun relativePath(): String {
        return path.toString()
    }

    override fun uri(): URI {
        return path.toUri()
    }
}