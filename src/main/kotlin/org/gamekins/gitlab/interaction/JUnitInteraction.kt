package org.gamekins.gitlab.interaction

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import org.gamekins.gitlab.GitLabParameters
import java.nio.file.Files
import java.nio.file.Paths

object JUnitInteraction {

    /**
     * Get all JUnit .xml files and aggregate test counts from them!
     *
     * Only successful tests are counted.
     */
    fun calcExecutedTestsFromJUnitXml(parameters: GitLabParameters): Int {
        val resultsDir = Paths.get(parameters.basePath, parameters.junitResultsPath)
        val tests = Files.newDirectoryStream(resultsDir, "*.xml")
            .map { Files.readString(it) }
            .sumOf { getTestCount(it) }

        return tests
    }

    private fun getTestCount(xmlContent: String): Int {
        val root = XmlMapper()
            .readTree(xmlContent)
        val tests = root.get("tests")?.textValue()?.toIntOrNull() ?: 0
        val errors = root.get("errors")?.textValue()?.toIntOrNull() ?: 0
        val skipped = root.get("skipped")?.textValue()?.toIntOrNull() ?: 0
        val failures = root.get("failures")?.textValue()?.toIntOrNull() ?: 0
        return 0.coerceAtLeast(tests - errors - skipped - failures)
    }
}