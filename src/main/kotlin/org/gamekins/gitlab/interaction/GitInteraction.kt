package org.gamekins.gitlab.interaction

import org.eclipse.jgit.lib.Constants.HEAD
import org.eclipse.jgit.revwalk.RevCommit
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import java.nio.file.Path

object GitInteraction {

    fun getHeadCommit(gitDirPath: Path): RevCommit {
        val builder = FileRepositoryBuilder()
        val repo = builder.setGitDir(gitDirPath.toFile()).setMustExist(true).build()
        val walk = RevWalk(repo)
        val head = repo.resolve(HEAD)

        return walk.parseCommit(head)
    }
}