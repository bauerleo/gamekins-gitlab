# Gamification of Software testing on GitLab

GitLab implementation of [Gamekins](https://github.com/jenkinsci/gamekins-plugin)

See the [thesis](thesis/thesis.pdf).

### Project requirements

- Java project
- Single module maven (or build your own docker image for gradle) project
- JUnit setup
- JaCoCo setup

### CI Integration

- Create dummy user with Access token
- Add dummy user as developer to project (or use --api-user-partof-leaderboard for real user token)
- Integrate gamekins-gitlab into your GitLab project by calling Gamekins as `after_script`
- Checkout `.gitlab-ci-example.yml`

### Environment variables set by GitLab CI:

```sh
CI_SERVER_URL=https://gitlab.com                 # used for API base path
CI_PROJECT_ID=123456                             # used for API
CI_JOB_URL=123456                                # used to link Job artifacts
CI_PROJECT_URL=https://gitlab.com/user/project   # used to link to Leaderboard Wiki page
```

### User defined environment variables with their default if they do not exist:

```sh
BASE_PATH=.                                      # used in development to access local project
CLASSES_FOLDER=target/classes                    # used to select random class
JACOCO_RESULTS_PATH=target/site/jacoco/          # JaCoCo test coverage reports
JUNIT_RESULTS_PATH=target/surefire-reports/      # JUnit test reports
```

### Necessary secret environment variables in GitLab project settings

Settings -> (CI/CD) -> Variables:

```sh
GITLAB_ACCESS_TOKEN=glpat-...  # secret GitLab access token
```

### CLI parameters:

```sh
--build-success $param, -s $param # used to check if CI run was success; $param == "success" means build was successful.
--api-user-partof-leaderboard # used if no dummy user is used, API access token user is part of leaderboard
```

## Development 

- set the environment and cli variables manually
- Run the main method of `GitLabRunner.kt`

### Build and push new Docker Container:

```shell
mvn clean package
docker build -t redevening/gamekins-gitlab:latest .
docker push redevening/gamekins-gitlab:latest
```


