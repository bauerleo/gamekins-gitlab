% !TeX spellcheck = de_DE
% !TeX encoding = UTF-8
% !TeX root = ../thesis.tex
\chapter{Background}\label{chap:background}

The Objective of this chapter is to supply the reader with the necessary background on Gamekins and GitLab. 


\section{Related Literature}

In order to evaluate and classify Gamekins, it is necessary to understand how gamification effects motivation and which other tools for gamification in software testing are available on the market.
Motivation is the entirety of motives of a person or an animal which lead to initiate, continue, or terminate a certain behavior. \cite[1087]{pschyrembel2002klinisches}

According to Juho Hamari “Gamification is defined as an intentional process of transforming practically any activity, system, service, product, or organizational structure into one which affords similar positive experiences, skills, and practices as found in games. This is commonly but optionally done with an intention to facilitate changes in behaviors or cognitive processes.” \cite{hamar2019gamification}

Carlos F. Barreto and César França reviewed 130 papers on gamification in software engineering \cite{barreto2021gamification}. 44 of these studies evaluated the effect of gamification on the user. 82 percent (36 studies) reported positive effects. 15 studies reported improvements in behavioral aspects, such as motivation, or acceptance of new practices. The top five game elements used in these studies have been points, badges, leaderboards, awards and quests. \cite{barreto2021gamification}

G. Martins de Jesus and F. Cutigi Ferrari reviewed 15 studies on gamification in software testing in 2018. \cite{de2018gamification} With one exception, the same studies have also been listed by Carlos F. Barreto and César França’s systematic literature review from 2021, who systematically screened the main sources for scientific papers: ACM Digital Library, IEEE Xplore and ScienceDirect.  \cite{barreto2021gamification}

Most of the 15 selected studies were applicable to both, educational and industrial context. All but one study defined concrete game elements, like points, badges, and leaderboards. The top pursued goals were to increase engagement, improve skills and to increase motivation. Out of the 15 studies, thirteen presented software prototypes for gamified testing. Three author groups used automated test generators for the gamification of their testing activities, like Code Defender \footnote{\url{https://code-defenders.org/}} or Randoop \footnote{\url{https://github.com/randoop}}.

In most studies, gamified conceptual frameworks have been developed. For example, the study of Bell et al. suggest to use gamification in education to develop positive habits with respect to software testing for the students in computer science courses and introduce HALO, a prototype for a learning environment. \cite{bell2011secret}

Based on this literature reviews, it can be concluded that previous research did not integrate the gamification of software testing framework into continuous integration tools like Jenkins or whole Collaboration platforms like Gitlab. This is a gap that Gamekins closes. 


\section{Gamekins Jenkins Plugin}
\label{sec:gamekinsJenkins}

Gamekins \footnote{\url{https://plugins.jenkins.io/gamekins/}} is a tool that gamifies software testing. It has been developed at the University of Passau, Chair of Software Development II in 2021. This section refers to the paper of Philipp Straubinger and Gordon Fraser \cite{straubinger2022gamekins} unless otherwise noted.

Gamekins was designed to motivate software developers to increase their efforts in software testing within the Jenkins continuous integration platform. Jenkins \footnote{\url{https://www.jenkins.io/}} is an open source automation and continuous integration/delivery server. It is a server-based system and supports version management tools, including git. The gamification of Gamekins is realized by software developers earning points for fulfilling certain test challenges and quests, which leads to competition with other developers as well as achievements that can be unlocked.

The Gamekins Jenkins plugin is called at the end of each build process of a software project. The checked-out status of the project is then analyzed. Test tasks, so-called challenges, are generated based on predefined rules and successful execution of the generated challenges is checked. The implementation was done as a Common Interface (CI) Plugin to Jenkins.

Gamekins currently supports Java or other JVM Languages based Projects. For checking failing Tests JUnit\footnote{\url{https://www.junit.org/}} must be used. For Test Coverage it uses JaCoCo \footnote{\url{https://www.jacoco.org/jacoco/}}.

The following seven types of challenges are currently supported by Gamekins:

\begin{itemize}
\item \textbf{Build Challenge}: Every time that the build fails, the developer gets the challenge to fix the build. If the challenge is solved successfully, the developer earns a reward of 1 point.

\item \textbf{Test Challenge}: The developer gets the task to write at least one more test. For any additional test, the developer is rewarded by 1 point.
\item \textbf{Class Coverage Challenge}: For a randomly selected target class, the developer has to cover more lines of code. If the coverage exceeds a predefined threshold (e.g., 80 percent) the user gets rewarded with 2 points, 1 point otherwise.

\item \textbf{Method Coverage Challenge}: For a selected method, the developer has to improve the test coverage. 2 points are awarded for high coverage (predefined threshold) and 1 point otherwise.

\item \textbf{Line Coverage Challenge}: For a selected line of code that is not covered yet, the developer has to improve coverage. The points depend on the overall coverage on the class, with 3 points if coverage is already high, and otherwise 2 points.

\item \textbf{Mutation Test Challenge}: Mutation tests are used to design new software tests and evaluate the quality of existing tests. Mutation testing makes a small change to the source code automatically. The changed version is called a mutant, and tests must be able to distinguish the original program from the mutant by the failing of a test. This is called killing the mutant. The surviving mutants can be used to design new tests. \cite{demillo1978hints}For the challenge, Gamekins creates a mutant of the code and the developer has to “kill a specific mutant by adding or modifying a test” \cite{straubinger2022gamekins}. These tests are rewarded with 4 points because of their difficulty. Gamekins uses Moco \footnote{\url{https://github.com/phantran/moco}} - a maven plugin developed for Gamekins - to generate mutants.

\item \textbf{Smell Challenge}: This challenge is created by analyzing the target class with SonarLint \footnote{\url{https://www.sonarlint.org/}}. SonarLint is a static code analysis tool to find characteristics in the source code that possibly indicates a deeper problem, so-called smells. Code smells can be detected in both source and test files. The developer will be rewarded by 1 to 4 points, based on the severity of the smell indicated by SonarLint.

\end{itemize}

Quests are set of three challenges. The sum of all points of the individual challenges plus three additional points are awarded when the quest is completed. The sub challenges represent quest steps that need to be solved one at a time. Only the current quest step challenge can be solved at one time.

Currently, there are nine predefined quests available in Gamekins.

The following quests are implemented in Gamekins with the order of the sub challenge steps:

\begin{itemize}
\item \textbf{Test Quest}: Three Test Challenges
\item \textbf{Smell Quest}: Three Smell Challenges
\item \textbf{Mutation Quest}: Three Mutation Challenges of the same class
\item \textbf{Package Quest}: Three Challenges that test files from the same package
\item \textbf{Class Quest}: Three Class Coverage Challenges of different classes
\item \textbf{Method Quest}: Three Method Coverage Challenges of the same class
\item \textbf{Line Quest}: Three Line Coverage Challenges of the same class
\item \textbf{Expanding Quest}: A Line, Method and Class Coverage Challenge of the same class
\item \textbf{Decreasing Quest}:A Class, Method and Line Coverage Challenge of the same class
\end{itemize}

Achievements reward certain behavior independently of challenges or quests. Gamekins has currently 63 types of achievements implemented, for example “Have ten tests in your project”. The level of difficulty of the achievements increases with each fulfilled achievement. Some achievements are only revealed after they have been achieved, while for other type of achievements the progress is visible. In some cases, the individual users benefit from solving achievements, and in other cases all users of a project benefit.

The project leaderboard serves to display the scores. The leaderboard shows the number of completed challenges and achievements, the points gained, the rank of each user of the project. Developers can be assigned to different teams, and the team leaderboard aggregates the scores per team.

The Jenkins build pipeline is defined in a Jenkins configuration file in the Groovy programming language. After a source code commit is made in the version control system, Jenkins starts a worker job in an isolated environment that contains only the current checked out project code. This job builds the project, executes the included tests in the source code and tracks the test coverage. Afterwards the Gamekins Jenkins Plugin is called which uses the information of the run and the newly generated test and coverage results to update the challenges, quests and achievements as well as the leaderboard. If there are open challenges, quests or unsolved achievements, Gamekins checks whether these are solved now and awards points to the solver. In the end, new challenges and quests are generated to replace the solved ones. If the remaining challenges are still solvable, they remain in place, otherwise the challenge gets deleted.

Gamekins provides its features only inside Jenkins. The Gamekins Challenges, Quests and Leaderboard can only be viewed from inside the Jenkins user interface. Jenkins only provides continuous integration, while GitLab provides a whole software suite to assist in software development - including continuous integration. For this reason, Gamekins should be extended to work on GitLab also without the need for Jenkins.

\section{GitLab Collaboration Platform}

GitLab \footnote{\url{https://www.gitlab.com}}is a web application for version management of software projects. It offers an issue tracking system, a system for continuous integration and continuous delivery, a wiki, a container registry as well as a security scan for containers and projectsource code. GitLab sees itself as ''The One DevOps Platform that empowers organizations to maximize the overall return on software development by delivering software faster and efficiently, while strengthening security and compliance.''\footnote{https://about.gitlab.com/company/} It allows ''control over all stages of the DevOps lifecycle'' \cite{gitlab10Q}.

GitLab has an estimated 30 million registered users, with 1 million being active licensed users. \cite[33]{gitlab10Q} It follows an open-core development model \footnote{\url{https://about.gitlab.com/pricing/}} where the core functionality is released under an open-source (MIT) license while the additional functionality such as code owners, multiple issue assignees, dependency scanning and insights are under a proprietary license. 

Below the features of GitLab which are relevant for this thesis are presented.

GitLab includes an Continuous Integration system that allows to continuously build and test software to assure the software quality. 

GitLab includes an issue management system. "An issue is either a task, new feature, user story,
or bug." \cite{bhat2017automatic}. Issues are always associated with a specific project. Issues have Metadata like assigned users and distinct labels. The labels are used to describe different categories of Issues.

GitLab includes Wiki functionality for each project. The project wiki is implemented as a separate \verb|git| repository where changes to the Wiki are tracked. The wiki allows the creation of \verb|markdown| formatted Wiki Pages that can be linked to each other.

All described features can be created and modified in the web interface, as well as the GitLab \verb|HTTP| based Application Programming Interface (API).

Using GitLab has the advantage that all steps of the software development process are included into one platform. Integrating Gamekins into it extends this functionality by providing gamification for software testing.

\section{GitLab Continuous Integration}

The GitLab Continuous Integration \footnote{\url{https://docs.gitlab.com/ce/ci/}} (CI) is based on a continuous integration pipeline that exectues steps automatically after new changes are published to GitLab by  pushing new \emph{git} commits. The steps of the pipeline can be individually configured to  execute sequentially or in parallel.

CI steps define how software projects are built, tests are run, build artifacts are packaged and can be used to deploy new versions of the built software.

The Continuous integration is configured by creating a \verb|.gitlab-ci.yml| file that can be stored in the \verb|git| repository of the project. It can also be configured directly in the GitLab User Interface (UI) without storing it in the project repository. \autoref{lst:gitlabCIExample} shows an example \verb|.gitlab-ci.yml| file that defines multiple pipeline steps.

Each step of the integration pipeline is run independently by a GitLab Runner. A GitLab Runner is an isolated environment, which is based on a configured Docker container with limited resources. A Docker container is a small contained isolated environment based which gives a high amount of isolation. Docker containers are based on Docker Images, that contain the filesystem that is available inside the Docker container. In the CI the project source code is checked out and user defined commands are executed while a shell is opened in the current working directory. Using Docker containers leads to reproducible builds as dependencies and system configuration are exactly the same on each pipeline run. \cite{boettiger2015introduction} A suitable Docker image can be used that only includes dependencies that needed for running the current pipeline step. The Docker image can be referenced by using the public docker registry \footnote{\url{https://hub.docker.com/}}, a private registry or the Docker registry that is included in GitLab.

\begin{lstlisting}[language=GitLabCI, caption=This listing shows an example .gitlab.yml that uses the maven image for compile and test steps and a simple bash image that executes a custom script.,label={lst:gitlabCIExample}]
stages:
  - compile
  - test
  - deploy

compile:
  image: maven:latest
  stage: compile
  script:
    - mvn compile

test:
  image: maven:latest
  stage: test
  script:
    -  mvn test

deploy:
  image: bash:latest
  stage: deploy
  script: ./deploy.sh
\end{lstlisting}