% !TeX spellcheck = de_DE
% !TeX encoding = UTF-8
% !TeX root = ../thesis.tex
\chapter{Design and Implementation}\label{chap:methods}

Gamekins is to date only available as a Jenkins plugin. The target of this thesis is to implement its functionality as a standalone application that can be integrated into the GitLab CI. Features of the Jenkins implementations will be available in the new implementation. The design will align to the existing Jenkins implementation to make it easier for Gamekins users and new users to adapt to the GitLab version. Some features have to be realized in a slightly different manner as the GitLab structure is different to Jenkins.

As a pre-condition for using Gamekins with a project, it has to be based on Java programming language and must be configured to create JaCoCo and JUnit reports after building the project. JUnit is a testing framework and JaCoCo is a test coverage tool for Java projects. 

The Achievements feature of the Jenkins version is excluded from transfer to Gitlab, because it is not possible to change user profiles of arbitrary users and achievements would not be able to assign to a specific person. Providing achievements to the whole team does not provide additional motivation to the single users. 

\autoref{sec:impl} describes the implementation of the Gamekins executable in detail. This Gamekins executable contains the business logic for the challenges and uses oter components provided by GitLab, mainly the GitLab CI, GitLab Issues and GitLab Wiki to interact with the user. It is packaged as a Docker container available on docker Hub\footnote{\url{https://hub.docker.com/repository/docker/redevening/gamekins-gitlab}} and can be integrated in the GitLab CI. When the GitLab CI is run it checks current quests and challenges and awards points to users if the challenges or quests are solved. The concept of challenges and quests is analog to the \verb|Gamekins| \verb|Jenkins| plugin.

For the Implementation to be able change GitLab resources the GitLab API\footnote{\url{https://docs.gitlab.com/ee/api/}} is used. To authenticate a GitLab Access token needs to be provided to the executable in the form of an environment variable.

For each challenge or quest an Issue is created in the GitLab Issue tracking system. If the challenge or quest is solved the GitLab Issue is closed and new challenges and quests are generated for users to solve by the Gamekins executable.

In the GitLab Wiki a \verb|Gamekins-Leaderboard| Wiki page displays which challenges are solved by users and gives a ranking of the users by scored points. This Wiki page also contains the current state about all currently open challenges and users.

\begin{figure}
  \includegraphics[width=\linewidth]{overview.drawio.png}
  \caption{This figure contains the main components of the Gamekins GitLab implementation.}
  \label{fig:overview}
\end{figure}

GitLab does not provide a feature to store status data like the scoring. It is a requirement to only use resources within Gitlab. For this reason, it is not feasable to use an external database. In order to by-pass this short coming, the internal state about open challenges and scoring has been hidden from users inside a \verb|HTML| comment in the Wiki Leaderboard page. \verb|JSON| was used to encode the state about current challenges and the users with their scoring data. \verb|JSON| is a lightweight text based data format which makes it a perfect solution for hiding persistent data in a text based Wiki page.\footnote{\url{https://www.ecma-international.org/publications-and-standards/standards/ecma-404/}}

\autoref{fig:overview} shows the main components of the Gamekins GitLab implementation.

The following sections describe the implementation in detail.

\section{Implementation of the Gamekins Executable}
\label{sec:impl}

The implementation of the standalone application was done in the Kotlin programming language because the original Gamekins Jenkins plugin was developed in Kotlin and there was no requirement to use a different language.

The implementation consists of 5 Kotlin packages which are be described below.

\subsection{Program Entrypoint and Main Package}

\begin{figure}
  \includegraphics[width=\linewidth]{process.drawio.png}
  \caption{This Figures shows the program flow of the GitLab Gamekins Executable}
  \label{fig:impl}
\end{figure}

The main package \verb|org.gamekins.gitlab| contains the program entry point \verb|GitLabRunner.kt| that reads environment variables and command line arguments, and saves them as a \verb|GitLabParameters| object.\newline It calls \verb|GitLabGamekins.execute()| to start the main program execution. The \verb|GitLabGamekins| class contains the main program logic.

The program flow is visualized in \autoref{fig:impl}. In the beginning it fetches the current saved state (open challenges and users) from the GitLab Wiki, checks if new project members have been added to the project and gets or creates the user that is the author of the latest commit.

Afterwards it checks all currently open challenges. This is a four step process. More details are in \autoref{sec:challenges}.

\begin{figure}
  \includegraphics[width=\linewidth]{congrazz.png}
  \caption{The solver of a challenge is congratulated for the accomplishment!}
  \label{fig:congrazz}
\end{figure}

\begin{enumerate}
  \item \textbf{Rejected Challenges:} \newline
  Users can reject challenges by closing the corresponding GitLab issue. These issues are removed from the open challenges. To notify users of the removal, a comment is created in the Issue page. 
  
  \item \textbf{Solved Challenges:} \newline
  Challenges are checked whether they are solved. If the current commit solves the challenge, then the author is awarded points depending on the challenges` difficulty. The Issue of the solved challenge is closed with a comment congratulating the user. Such congratulations are shown in \autoref{fig:congrazz}.

  \item \textbf{Unsolvable Challenges} \newline
  There are situations that make challenges unsolvable, like deleting a file which is associated with a challenge. These challenges are rejected automatically and GitLab Issues of these challenges are deleted with a comment.

  \item \textbf{Solved Quest step (for Quests only} \newline
  Quests contain multiple sub challenges but only the current quest step of the quest can be solved. If this sub challenge step is solved then the quest is incremented to the next quest step. The challenge GitLab Issues description is updated accordingly.
\end{enumerate}

After the open challenges and quests have been checked new challenges are generated with issues in GitLab.

At the end the Leaderboard wiki page is updated containing a list of players, their scores and solved challenges. In this Leaderboard wiki page the current state about open challenges and users. The status data is updated in the same Wiki page.

\subsection{Challenges Package}

The \verb|org.gamekins.gitlab.challenge| package contains all challenges as well as logic regarding creation of challenges.

All challenges have to implement the newly created \verb|GChallenge| interface. Its important methods are \verb|isSolved()| and \verb|isSolvable| define when the challenge is solved and when the challenge is no longer solvable. The user is awarded the amount of points defined in the \verb|getScore()| method.

Challenges are generated by the \verb|GChallengeFactory| class. It contains a Map with different challenge integer weights, that allows for some challenges to occur more frequently than others. It generates Challenges randomly based on these weights.

\subsection{Quests Package}

The \verb|org.gamekins.gitlab.challenge.quest| package implements quests. Quests are special Challenges that contain multiple sub challenge steps. Because of that the quest interface \verb|GQuest| implements the challenge interface \verb|GChallenge|. The actual quest implementation is in the \verb|GMultiChallengeQuest| class that implements both interfaces. It provides the \verb|isStepSolved()| method as well as an assigned user. Because all quests just differ in their sub challenges, they are all represented by the \verb|GMultiChallengeQuest| class.

\subsection{GitLab Package}

The \verb|org.gamekins.gitlab.gitlab| package provides access to the GitLab Resources. File \verb|GitLabIssue| creates, comments and deletes GitLab issues.\verb|GStore| and \verb|GLeaderboard| handle fetching and storing of the leaderboard as well as saving the internal \verb|JSON| State as hidden \verb|HTML| comment in the Wiki.


\subsection{Interaction Package}

In the \verb|org.gamekins.gitlab.interaction| package local files of the project under test are accessed. \verb|JaCoCoInteraction| is responsible for evaluating JaCoCo Reports, \verb|JUnitInteraction| evaluates JUnit reports. These reports give information about the test coverage and the number of tests. The \verb|GitInteraction| file can get information about the current commit with the important information being its author.

\section{GitLab CI integration}
\label{sec:ci}

Gamekins-GitLab is provided as a docker container\footnote{\url{https://hub.docker.com/repository/docker/redevening/gamekins-gitlab}}. The Docker container is based on the maven docker image, so it can be used both for building \verb|maven| based \verb|jvm| applications as well as running the Gamekins-GitLab executable that interacts with the project under test and interacts with GitLab.

To integrate Gamekins-GitLab into your build process, you have to integrate it into the \verb|.gitlab-ci.yml| file as an \verb|after_script| so that it is executed after the build process in the Continuous Integration pipeline.

To interact with GitLab a GitLab Access Token with the \verb|api| permission scope\footnote{\url{https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html}} has to be provided as an environment variable. This variable is needed to interact with Issues and the Wiki in GitLab. GitLab already provides an available \verb|CI_JOB_TOKEN| access token. However, This pre-defined token cannot be used as it does not provide the necessary \verb|api| permissions to change GitLab Issues and Wiki pages. For security reasons, it is recommended to add a dummy user to your GitLab project whose access token is used for this purpose. This dummy user will create Issues for challenges and quests, and will update the Leaderboard Wiki page. To make sure that the dummy user's access token is not available publicly it should not be configured directly in the \verb|.gitlab-ci.yml| file, but be provided as a project, group or instance variable. Per default the dummy user whose access otken is used is excluded from the Leaderboard.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=GitLabCI, caption=Gamekins-GitLab integration into .gitlab-ci.yml, label=lst:gamekinsConfig]
stages:
  - package

cache:
  paths:
    - .m2/repository/
variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

  #####################################################################
  #                 Gamekins-GitLab environment variables             #
  #####################################################################
  # CI_SERVER_URL: Set by GitLab CI, used as API base url
  # CI_PROJECT_ID Set by GitLab CI, used for API
  # CI_JOB_STATUS Set by GitLab CI, used to determine if the build failed or was a success (Build Challenge)
  # CI_JOB_URL Set by GitLab CI, used to link to artifacts
  # CI_PROJECT_URL Set by GitLab CI, used to link to wiki Leader Board
  # GITLAB_ACCESS_TOKEN  To be set project secrets, used for API access
  JUNIT_RESULTS_PATH: "target/surefire-reports/"
  JACOCO_RESULTS_PATH: "target/site/jacoco"

package:
  stage: package
  image: redevening/gamekins-gitlab:latest
  script:
    - mvn clean package
  after_script:
    - java -jar /gamekins-gitlab.jar -s $CI_JOB_STATUS
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - $JACOCO_RESULTS_PATH
    # - Other files to include in the artifact
\end{lstlisting}
\end{minipage}

As Gamekins-GitLab uses JUnit and JaCoCo reports, you can configure paths to these built reports as environment variables in the \verb|.gitlab-ci.yml| configuration file.

Other environment variables that are used are predefined by the GitLab CI. The \verb|CI_JOB_STATUS| predefined CI variable is used to determine whether the build was successful. It has to be used as a \verb|cli| argument \verb|-s| which allows to integrate gamekins-gitlab into other CI solutions than the integrated GitLab CI.

This is a list of all used environment variables:

\begin{itemize}
  \item \verb|CI_SERVER_URL|: Set by GitLab CI, used as API base url
  \item \verb|CI_PROJECT_ID|: Set by GitLab CI, used for API access to the project
  \item \verb|CI_JOB_STATUS|: Set by GitLab CI, used to determine if the build failed or was a success
  \item \verb|CI_JOB_URL|: Set by GitLab CI, used to link to reports in Issue comments
  \item \verb|CI_PROJECT_URL|: Set by GitLab CI, used to link to wiki Leader Board
  \item \verb|JUNIT_RESULTS_PATH|: User provided path to JUnit report folder \newline (default: \verb|"target/surefire-reports/"|)
  \item \verb|JACOCO_RESULTS_PATH|: User provided path to JaCoCo reports \newline (default: \verb|"target/site/jacoco"|)
  \item \verb|GITLAB_ACCESS_TOKEN|: API access token, to be set as secret CI variable as described above
\end{itemize}

To be able to link to specific \verb|JaCoCo| reports in Challenge Issues, \verb|JaCoCo| reports need to be defined as build artifacts\footnote{\url{https://docs.gitlab.com/ce/ci/pipelines/job_artifacts.html}}. This also has to be defined in the \verb|.gitlab-ci.yml| configuration file.

\autoref{lst:gamekinsConfig} is an example \verb|.gitlab-ci.yml| configuration that can be used to integrate GitLab-Gamekins into your project. It also caches maven dependencies between build cycles:

\section{User Management}

The users/players must be members of the project under test in GitLab. These can be defined in the GitLab UI. Gamekins checks the latest \verb|git| commit if challenges are solved. If the commit author is not part of the project members, then the author is added as an additional user. 

GitLab matches GitLab users and \verb|git| commit authors by E-Mail. The GitLab API per default does not allow to view E-Mails of arbitrary users. Because of that, Gamekins matches GitLab users and \verb|git| commit authors by comparing its name with the GitLab name and username. Discrepancies might arise if a different name is used in the \verb|git| commit than the GitLab name or username.

All users currently recognized by Gamekins can be viewed in the project Leaderboard. Users are uniquely identified by their \verb|id|. For GitLab project members this is the GitLab internal id integer. For users that are created from the \verb|git| commit a random negative integer is chosen the first time they are encountered as commit author to make sure that the chosen integer does not conflict with the positive internal GitLab ids.

\section{State Management}
\label{sec:state}

Gamekins shows current Challenges and Quests in GitLab Issues and shows the current Leaderboard as a GitLab Wiki page.

We store information about the Leaderboard, users and their solved challenges, and all currently open challenges in an internal state. This internal state is converted into \verb|JSON| and stored in the Wiki Leaderboard page as an \verb|HTML| comment. When the application is started it tries to fetch the Wiki page \verb|Gamekins-Leaderboard| and tries to load the \verb|JSON| state from it. If the Wiki page does not exist or the extration fails an empty internal state is initialized.

This internal state, represented by the \verb|GStoreContent| class, is handled by the \verb|GStore| object. The internal state contains a list of all open Challenges in the \verb|openChallenges| property as well as information about all users in the \verb|users| property.

To manually reset the internal state, you can delete the \verb|Gamekins-Leaderboard| GitLab Wiki page. On the next run a new empty state gets initialized and the Leader Board gets recreated.

\begin{minipage}{\linewidth}
\begin{lstlisting}[language=GitLabCI, caption=This listing shows the JSON representation of the internal state, label=lst:json]
{
  openChallenges: [{
      "type": "GClassCoverageChallenge",
      "id": "35",
      "classPackage": "org.proto",
      "className": "Beltalowda",
      "initialCoverage": {
        "missed": 5,
        "all": 91,
        "coverageReportUrl": ".../org.proto/Beltalowda.html",
    }},{
      "type": "GTestCountChallenge",
      "id": "36",
      "initialTestCount": 42
    }, ...
  ],
  users: [{
    id: 123456,
    name: "Josephus_Miller",
    userName: "agentMiller",
    score: 12,
    solvedChallengeIds: [8,13,21,34]
    }, ...
  ]
}
\end{lstlisting}
\end{minipage}

The \verb|JSON| state is set up in the following way:

Challenges are represented as objects. The challenge has a  \verb|type| property which is used to select the corresponding Challenge class to instantiate. Each challenge type has a different set of properties that are used to describe it. For example, a simple Test Count Challenge only needs information about the current number of tests, a Line Coverage Challenge needs information about which line to cover in which file.

Users are also represented as objects. The properties of a user object are its \verb|id|, \verb|name| and \verb|userName|, her \verb|score|, whether she is a project member and most importantly a list of \verb|solvedChallengeIds|. The solved challenge ids refer to all challenges the user has solved. Because the challenge id is the same as the corresponding GitLab issue id, this project specific id can be used to generate Urls to the corresponding GitLab Issues.

\autoref{lst:json} shows the representation of the internal state encoded in \verb|JSON|.

\section{Challenges}
\label{sec:challenges}

\begin{figure}
  \includegraphics[width=\linewidth]{issue-list.png}
  \caption{Multiple Challenge and Quest Issues}
  \label{fig:issueList}
\end{figure}

Challenges are the core part of Gamekins. A challenge is task generated to be solved by users that increases test metrics. To users in the GitLab project challenges are presented as GitLab Issues. These issues describe tasks that are necessary for the challenge to be solved. As GitLab Issues are also used for general project managment and bug tracking, Issues created by Gamekins have the \verb|gamekins:challenge| tag. This tag allows users to filter out or search for Gamekins Issues. 

For solving a challenge the solver is awarded points depending on the challenge difficulty. The difficulty depends on the different challenge types and on other factors like overall test coverage. When the challenge is solved, the GitLab Issue is automatically assigned to the solver in GitLab and the Issue is subsequently closed with a comment congratulating the user for the success. \autoref{fig:congrazz} shows such a congratulatory message.

If the current state of the project makes it impossible to solve the challenge, then the corresponding GitLab Issue is automatically closed with a comment.

If users don’t like the current challenges, they can close open Gamekins GitLab Issues to indicate that they reject the challenge and want new ones to be generated. On the next Gamekins CI run new challenges are generated.

The maximum number of open challenges is based on the number of project members. This is different to the original Jenkins version of Gamekins. In the Gamekins Jenkins plugin the challenges are personalized. Each user has a fix number of three challenges. 

In the Gitlab version it is not necessary to personalize the challenges. This means, the whole project team can work together on all challenges. For this reason, only two challenges are generated for each project member with a global maximum of six open challenges.

Challenges are generated based on the above-mentioned rules, with on exception: If the current CI run and build was not successful a \textbf{Build Challenge} is always generated even if the maximum number of open challenges is hit.

All other Challenge types are randomly selected. Each challenge type has an integer weight that represents how often it should be generated relative to other challenge types.

If a challenge is solved the author of the latest commit is the challenge's solver and gets the score points of the challenge as a reward! Challenges can be solved by all users on a first come first serve basis. The first commit/CI-run that solves a challenge gets the points!

\begin{minipage}{\linewidth}
\begin{itemize}
   \item \textbf{Test Challenge}: Weight of 1 (7\%)
   \item \textbf{Class Coverage Challenge}: Weight of 2 (14\%)
   \item \textbf{Method Coverage Challenge} Weight of 3 (21\%)
   \item \textbf{Line Coverage Challenge}: Weight of 4 (29\%)
    \item \textbf{Smell Challenge}: Weight of 4 (29\%)
\end{itemize}
\end{minipage}

The algorithm to select a challenge type works in the following way: The weights are partially summed up and stored in a \verb|TreeMap| (1-Test Challenge, 3-Class Coverage Challenge, 6-Method Coverage Challenge, 10-Line Coverage Challenge, 14-Smell Challenge). A random number between zero and the weight sum (14) is chosen. Then this random number is used in a binary search to get the corresponding challenge type. For example the random number four would lead to a class coverage challenge.

\subsection{Build Challenge}

\begin{figure}
  \includegraphics[width=\linewidth]{build-challenge.png}
  \caption{A Build Challenge GitLab Issue}
  \label{fig:buildChallenge}
\end{figure}

A Build challenge is created if the build failed. This is determined when the Gamekins executable is not called with the successfull build \verb|cli| argument \verb|-s success|. When the Gamekins executable is integrated in the \verb|.gitlab-ci.yml| the build status \verb|success| is automatically set by the GitLab CI via an environment variable. See \autoref{sec:ci}. There can only exist one Build Challenge.

\autoref{fig:buildChallenge} shows a Build Challenge GitLab Issue.

\textbf{Conditions to be solved:}

This Build Challenge is solved if another commit makes the CI run without errors. The Build challenge is only created if no other build challenge exists.

\textbf{Conditions to be not solvable anymore:}

A Build Challenge is always solvable.

\textbf{Awarded score points:}

The solved Build Challenge is awarded with one score point!

\subsection{Test Challenge}

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{test-challenge.png}
  \caption{A Test Challenge GitLab Issue}
  \label{fig:testChallenge}
\end{figure}

The simple Test Challenge gives users the challenge to increase the number of current tests by one. When the challenge is generated all the \verb|JUnit| test \verb|xml| reports are aggregated, counted and saved in the internal state as an initial test count.

\autoref{fig:testChallenge} shows a Test Challenge GitLab Issue.

\textbf{Conditions to be solved:}

The Test Challenge is solved if a new CI run is run successful with more \verb|JUnit| tests than the initial test count.

\textbf{Conditions to be not solvable anymore:}

A Test Challenge is always solvable.

\textbf{Awarded score points:}

The solved Test Challenge is awarded with one score point!

\subsection{Class Coverage Challenge Issue}

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{class-challenge.png}
  \caption{A Class Coverage Challenge GitLab Issue}
  \label{fig:classChallenge}
\end{figure}

When it is determined that a Class Coverage Challenge should be created, a random \verb|jvm| class is chosen by choosing a random class coverage file in the configured \verb|JaCoCo| reports folder. The report has to show that not all instructions of the class are already covered.

This coverage information from this file is used to set a internal \verb|CoverageInformation| object that contains information about the number all instructions of the class, the number of missed instructions of the class and a calculated Url to the \verb|JaCoCo| report artifact that is accessible from GitLab and is provided to the user in the Issue description. From the coverage information a coverage percentage is calculated that is also put into the Issue description.

GitLab has an integrated feature to display coverage information when it is provided by the \verb|cobertura|\footnote{\url{http://cobertura.github.io/cobertura/}} coverage tool. While it would be possible to convert the \verb|JaCoCo| report into a \verb|cobertura| report, the GitLab feature for displaying coverage information is only availabe in Merge Requests, not Issues. \footnote{\url{https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html}} For this reason, it wa not possible to use the cobertura coverage tool.

\autoref{fig:classChallenge} shows a Class Coverage Challenge GitLab Issue.

\textbf{Conditions to be solved:}

The Class Coverage Challenge is solved if a new CI run is successful which increases the total coverage percentage of the class under test to be higher than the initial coverage information in the internal state. This coverage percentage is calculated in the same way as the initial coverage.

\textbf{Conditions to be not solvable anymore:}

The Class Coverage Challenge is not solvable anymore if the \verb|JaCoCo| report for the class under test does not exist anymore (class does not exist anymore) or the line to cover cannot be covered anymore.

\textbf{Awarded score points:}

The solved Class Coverage Challenge is awarded with two score points if the initial class coverage is over 80\%, otherwise one score point is given for the solved challenge.


\subsection{Method Coverage Challenge}

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{method-challenge.png}
  \caption{A Method Coverage Challenge GitLab Issue}
  \label{fig:methodChallenge}
\end{figure}

In the same way as for the Class coverage challenge a random class under test is chosen. Next a random method of this class is selected that does not have full coverage.

The Method Coverage Challenge is similar to the Class Coverage challenge but it only looks into into the coverage of a single method. Similar to class coverage \verb|JaCoCo| provides method coverage in their reports out of the box. The internal \verb|CoverageInformation| object is created in a similar way. The GitLab Issue of the Challenge contains a section of the method source code as a reference to the user. The same \verb|JaCoCo| report as in the Class Coverage Challenge is provided as a Link additionally.

\autoref{fig:methodChallenge} shows a Method Coverage Challenge GitLab Issue.

\textbf{Conditions to be solved:}

Similar to the other coverage challenges the challenge is solved if the coverage increases in a successful CI run.

\textbf{Conditions to be not solvable anymore:}

The Method Coverage Challenge is no longer solvable if the method does not exist anymore or has no lines to cover.

\textbf{Awarded score points:}

The solved Method Coverage Challenge is awarded with two score points if the coverage of the class under test is over 80\%, otherwise one score point is given for the solved challenge.


\subsection{Line Coverage Challenge}

\begin{figure}
  \includegraphics[width=\linewidth]{line-challenge.png}
  \caption{Line Coverage Challenge}
  \label{fig:lineChallenge}
\end{figure}

The Line Coverage Challenge is similar to the other coverage challenges but checks only the coverage of a single line. To get line coverage information the \verb|JaCoCo| report that shows all lines of a class is used. From this report all lines that can actually be covered by a test are taken into account for the random selection. A Link to this detailed line by line report is provided in the GitLab Issue.

\autoref{fig:lineChallenge} shows a Line Coverage Challenge GitLab Issue.

\textbf{Conditions to be solved:}
Similar to the other coverage challenges the challenge is solved if the coverage increases in a successful CI run.

\textbf{Conditions to be not solvable anymore:}
The Line Coverage Challenge is not solvable anymore if the class does not exist anymore or the the line to cover cannot be covered anymore.

\textbf{Awarded score points:}

As the other Coverage Challenges the solved Line Coverage Challenge is awarded with two score points if the coverage of the class under test is over 80\%, otherwise one score point is given for the solved challenge.

\subsection{Smell Challenge}

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{smell-challenge.png}
  \caption{A Smell Challenge GitLab Issue}
  \label{fig:smellChallenge}
\end{figure}

The Smell challenge uses static Analysis on the Java Source Code to find code smells. Static Source Code Analysis tools allows to "look for violations of reasonable or recommended programming practice". These violations or smells may indicate program bugs and can lead to faults.\cite{ayewah2008using} In the same way as the Gamekins Jenkins Plugin we use SonarLint\footnote{\url{https://www.sonarlint.org/}} to detect the code smells.

The GitLab Issue of the generated challenge includes a Link to the SonarLint web page that gives more detail about the found SonarLint rule that was not satisfied. An example rule is that local variables should not shadow class fields\footnote{\url{https://rules.sonarsource.com/java/RSPEC-1117}}.

\autoref{fig:smellChallenge} shows a Smell Challenge  GitLab Issue.

\textbf{Conditions to be solved:}
The challenge is solved if the underlying problem found by the static analysis is fixed.

\textbf{Conditions to be not solvable anymore:}
The challenge is not solvable anymore if the class of the found problem does not exist anymore or if the lines that show the problem do not exist anymore.

\textbf{Awarded score points:}

The points awarded depend on the severity of the found issues. SonarLint knows the severities \verb|MINOR|, \verb|MAJOR|, \verb|CRITICAL| and \verb|BLOCKER| that are awarded one to four points depending on the severity\footnote{\url{https://docs.sonarqube.org/latest/user-guide/issues/}}. Issues of type \verb|INFO| are completely ignored and are not used in challenges.

\subsection{Mutation Challenge (not implemented)}
\label{sec:mutation}

The Gamekins Jenkins Plugin provides a Mutation Challenge. Due to the complexity of implementing the challenge as well as the fact that projects Gamekins is called on need to include a extra maven plugin Moco\footnote{\url{https://github.com/phantran/moco}} the Mutation Test Challenge is not implemented in the GitLab Gamekins version.

\subsection{Quest Challenge}

\begin{figure}[!htb]
  \includegraphics[width=\linewidth]{quest.png}
  \caption{An expanding Quest GitLab Issue that shows the detailed description of the current quest step, a line coverage challenge.}
  \label{fig:quest}
\end{figure}

Quest Challenges are special challenges that include three sub challenges to solve. The user who has solved all sub challenges is awarded the sum of the points of the sub challenges as well as three extra points. The sub challenges to solve represent challenge steps that need to be solved one after another. The GitLab Issue that represents the quest shows a detailed description of the current challenge step as well as a list of the titles of all the quest steps to be solved. Only one Quest step can be solved per CI run and the detailed description of the other quest steps is not revealed until they are the current quest step.

Once a user solves the first quest step the quest is now assigned to this user. The remaining quest steps can only be solved by the assigned user. If another user solves the quest step it is not counted for the quest challenge. However is the assigned user makes an unrelated commit later the current quest step is now solved. This is a limitation that also exists in the Gamekins Jenkins Plugin.

As a Quest is just a challenge that includes sub challenges the Quest is also represented as a GitLab Issue.

The GitLab Gamekins version includes the same kind of quests as the Gamekins Jenkins Plugin described in \autoref{sec:gamekinsJenkins} except Mutation Quests.

\autoref{fig:quest} shows a Expanding Quest GitLab Issue.

\section{Leaderboard}

\begin{figure}
  \includegraphics[width=\linewidth]{leaderboard.png}
  \caption{Wiki Leaderboard Page}
  \label{fig:leaderboard}
\end{figure}

The Leaderboard is a dedicated Page in the GitLab project Wiki and lists users with their awarded points and links to their solved challenges GitLab Issues. The Leaderboard page is recreated after each Gamekins run.

Links to current open challenge GitLab Issues are displayed at the top of the Leaderboard page.

The Leaderboard table contains a list of all users known to Gamekins. This includes all GitLab project members as well as users no GitLab account can be associated with. The table has columns for the users avater, full name, GitLab user name, whether the user is a project member, the user's score and links to the solved challenges. If the user has no associated GitLab Account a random Avatar is chosen from the open source Dicebear\footnote{\url{https://avatars.dicebear.com/}} project. The used avatars are provided under a creative commons license.

The Leaderboard Wiki page also includes the internal state described in \autoref{sec:state}

\autoref{fig:leaderboard} shows an example Leaderboard page.

\section{Achievements (not implemented)}
\label{sec:achievements}

The Achievements feature of the Gamekins Jenkins Plugin is not implemented in the Gamekins GitLab version.

In the Jenkins implementation users are rewarded little achievement images that show their accomplishments. As the GitLab API provides no way to alter the profile page of other users to prevent security issues the only sensible option to display these achievements in the Leaderboard.

An handling of Achievements like this is possible but it would extend the Leaderboard with achievement information of all users as well as store all the achievement images. To not bloat the Leaderboard Wiki page that already contains the internal state, it was decided to not provide the achievements feature in the GitLab implementation of Gamekins.


